<?php

namespace App\Providers;

use App\Http\Controllers\Categories;
use App\Http\Controllers\Search;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->menuCategoriesLoadToIndex();
        $this->searchBarLoadToIndex();
        $this->menuCategoriesLoadToApp();
        $this->searchBarLoadToApp();
    }

    public function menuCategoriesLoadToApp()
    {
        View::composer('app', function ($view){
            $view->with('categories', Categories::getCategories());
        });
    }

    public function searchBarLoadToApp()
    {
        View::composer('app', function ($view)
        {
            $view->with('search', Search::searchBlock());
        });
    }

    public function menuCategoriesLoadToIndex()
    {
        View::composer('index', function ($view){
            $view->with('categories', Categories::getCategories());
        });
    }

    public function searchBarLoadToIndex()
    {
        View::composer('index', function ($view)
        {
            $view->with('search', Search::searchBlock());
        });
    }
}
