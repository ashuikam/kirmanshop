<?php

namespace App\Http\Controllers;


use Aimeos\MW\View\Helper\Encoder\Standard;
use Illuminate\Http\Request;


use Illuminate\Support\Facades\App;
use \Aimeos\Controller\Frontend;
use Illuminate\Support\Facades\Response;


class Categories extends Controller
{
    public static function getCategories ()
    {
        $context = App::make('aimeos.context')->get(false);
        $manager = \Aimeos\MShop::create( $context, 'locale' );
        $item = $manager->bootstrap( 'default', 'ru', 'BYN', true );
        $context->setLocale( $item );
        $tree = Frontend::create( $context, 'catalog' )->getTree();

        return Response::view( 'shop::catalog.categories', [ 'categories' => $tree ] );
    }
}
