<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class Search extends Controller
{
    public static function searchBlock()
    {
        return Response::view('shop::catalog.search');
    }
}
