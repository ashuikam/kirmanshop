<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Search;

class Main extends Controller
{
    public function index()
    {
        return Response::view('index');
    }

    public function about()
    {
        return Response::view('shop::about');
    }

    public function contacts()
    {
        return Response::view('shop::contacts');
    }
}
