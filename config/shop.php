<?php

return [

	'apc_enabled' => false, // enable for maximum performance if APCu is availalbe
	'apc_prefix' => 'aimeos:', // prefix for caching config and translation in APCu
	'pcntl_max' => 4, // maximum number of parallel command line processes when starting jobs

	'routes' => [
		// Docs: https://aimeos.org/docs/Laravel/Custom_routes
		// Multi-sites: https://aimeos.org/docs/Laravel/Configure_multiple_shops
		// 'admin' => ['prefix' => 'admin', 'middleware' => ['web']],
		// 'jqadm' => ['prefix' => 'admin/{site}/jqadm', 'middleware' => ['web', 'auth']],
		// 'jsonadm' => ['prefix' => 'admin/{site}/jsonadm', 'middleware' => ['web', 'auth']],
		// 'jsonapi' => ['prefix' => 'jsonapi', 'middleware' => ['web', 'api']],
		// 'account' => ['prefix' => 'myaccount', 'middleware' => ['web', 'auth']],
		// 'default' => ['prefix' => 'shop', 'middleware' => ['web']],
		// 'update' => [],
	],


	'page' => [
		// Docs: https://aimeos.org/docs/Laravel/Adapt_pages
		// Hint: catalog/filter is also available as single 'catalog/tree', 'catalog/search', 'catalog/attribute'
		'account-index' => [ 'account/profile','account/subscription','account/history','account/favorite','account/watch','basket/mini','catalog/session','locale/select' ],
		'basket-index' => [ 'basket/standard','basket/related' ],
		'catalog-count' => [ 'catalog/count' ],
		'catalog-detail' => [ 'basket/mini','catalog/stage','catalog/detail','catalog/session','locale/select', 'catalog/tree'],
		'catalog-list' => [ 'basket/mini','catalog/filter','catalog/stage','catalog/lists','locale/select', 'catalog/search', 'catalog/tree' ],
		'catalog-stock' => [ 'catalog/stock' ],
		'catalog-suggest' => [ 'catalog/suggest' ],
		'catalog-tree' => [ 'basket/mini','catalog/filter','catalog/stage','catalog/lists' ],
		'checkout-confirm' => [ 'checkout/confirm' ],
		'checkout-index' => [ 'checkout/standard' ],
		'checkout-update' => [ 'checkout/update' ],
	],


	/*
	'resource' => [
		'db' => [
			'adapter' => config('database.connections.mysql.driver', 'mysql'),
			'host' => config('database.connections.mysql.host', '127.0.0.1'),
			'port' => config('database.connections.mysql.port', '3306'),
			'socket' => config('database.connections.mysql.unix_socket', ''),
			'database' => config('database.connections.mysql.database', 'forge'),
			'username' => config('database.connections.mysql.username', 'forge'),
			'password' => config('database.connections.mysql.password', ''),
			'stmt' => ["SET SESSION sort_buffer_size=2097144; SET NAMES 'utf8mb4'; SET SESSION sql_mode='ANSI'"],
			'limit' => 3, // maximum number of concurrent database connections
			'defaultTableOptions' => [
					'charset' => config('database.connections.mysql.charset'),
					'collate' => config('database.connections.mysql.collation'),
			],
		],
	],
	*/

	'admin' => [],

	'client' => [
		'html' => [
            'email' => [
                'from-email' => 'manager@malyshkin.by',
                'from-name' => 'manager',
                'bcc-email' => [
                    'manager@malyshkin.by',
                ],
                'payment' => [
                    'html' => [
                        'common' => [
                            'content' => [
                                'baseurl' => 'https://malyshkin.by/',
                            ],
                            'template' => [
                                'baseurl' => public_path('css'),
                            ],
                        ],
                        'standard' => [
                            'template-body' => [public_path('email/payment/html-body-standard')],
                        ],
                    ],
                ],
            ],
			'basket' => [
				'cache' => [
					'enable' => false, // Disable basket content caching for development
				],
			],
			'catalog' => [
			    'lists' => [
			        'size' => 18,
                ],
			    'filter' => [
//			        'attribute' => [
//			            'types' => [
//			                'Втулки',
//                            'Длина коляски',
//                        ],
//                    ],
			        'tree' => [
			            'levels-always' => 2,
                    ],
                ],
			    //'cache' => ['enable' => false], //for development
				'selection' => [
					'type' => [
						'color' => 'radio',
						'length' => 'radio',
						'width' => 'radio',
					],
				],
			],
            'checkout' => [
                'standard' => [
                    'address' => [
                        'countries' => [
                            'BY'
                        ],
                        'states' => [
                            'BY' => [
                                'GM' => 'Гомельская',
                                'MG' => 'Могилевская',
                                'MS' => 'Минская',
                                'GD' => 'Гродненская',
                                'VT' => 'Витебская',
                                'BT' => 'Брестская',
                            ],
                        ],
                        'delivery' => [
                            'mandatory' => [
                                '0' => 'order.base.address.firstname',
                                '1' => 'order.base.address.lastname',
                                '2' => 'order.base.address.address1',
                                '3' => 'order.base.address.city',
                                '4' => 'order.base.address.languageid',
                                '5' => 'order.base.address.telephone',
                                '6' => 'order.base.address.email',
                            ],
                            'hidden' => [
                                '0' => 'order.base.address.countryid',
                            ],
                        ],
                        'billing' => [
                            'mandatory' => [
                                '0' => 'order.base.address.firstname',
                                '1' => 'order.base.address.lastname',
                                '2' => 'order.base.address.address1',
                                '3' => 'order.base.address.city',
                                '4' => 'order.base.address.languageid',
                                '5' => 'order.base.address.telephone',
                                '6' => 'order.base.address.email',
                            ],
                            'hidden' => [
                                '0' => 'order.base.address.countryid',
                            ],
                        ],
                    ],
                ],
            ],
		],
	],
	'controller' => [
	    'jobs' => [
	        'product' => [
	            'import' => [
	                'csv' => [
	                    'mapping' => [
	                        'item' => [
	                            '0' => 'product.code',
                                '1' => 'product.label',
                                '2' => 'product.type',
                                '3' => 'product.status'
                            ],
                            'text' => [
                                '4' => 'text.type',
                                '5' => 'text.content',
                                '6' => 'text.type',
                                '7' => 'text.content'
                            ],
                            'media' => [
                                '8' => 'media.url',
                            ],
                            'price' => [
                                '9' => 'price.currencyid',
                                '10' => 'price.quantity',
                                '11' => 'price.value',
                                '12' => 'price.taxrate'
                            ],
                            'attribute' => [
                                '13' => 'attribute.type',
                                '14' => 'attribute.code',
                                '15' => 'attribute.type',
                                '16' => 'attribute.code',
                                '17' => 'attribute.type',
                                '18' => 'attribute.code',
                                '19' => 'attribute.type',
                                '20' => 'attribute.code',
                                '21' => 'attribute.type',
                                '22' => 'attribute.code',
                                '23' => 'attribute.type',
                                '24' => 'attribute.code',
                                '25' => 'attribute.type',
                                '26' => 'attribute.code',
                                '27' => 'attribute.type',
                                '28' => 'attribute.code',
                                '29' => 'attribute.type',
                                '30' => 'attribute.code',
                                '31' => 'attribute.type',
                                '32' => 'attribute.code',
                                '33' => 'attribute.type',
                                '34' => 'attribute.code',
                                '35' => 'attribute.type',
                                '36' => 'attribute.code',
                                '37' => 'attribute.type',
                                '38' => 'attribute.code',
                                '39' => 'attribute.type',
                                '40' => 'attribute.code',
                                '41' => 'attribute.type',
                                '42' => 'attribute.code',
                                '43' => 'attribute.type',
                                '44' => 'attribute.code',
                                '45' => 'attribute.type',
                                '46' => 'attribute.code',
                                '47' => 'attribute.type',
                                '48' => 'attribute.code',
                                '49' => 'attribute.type',
                                '50' => 'attribute.code',
                                '51' => 'attribute.type',
                                '52' => 'attribute.code',
                                '53' => 'attribute.type',
                                '54' => 'attribute.code',
                                '55' => 'attribute.type',
                                '56' => 'attribute.code',
                                '57' => 'attribute.type',
                                '58' => 'attribute.code',
                                '59' => 'attribute.type',
                                '60' => 'attribute.code',
                                '61' => 'attribute.type',
                                '62' => 'attribute.code',
                                '63' => 'attribute.type',
                                '64' => 'attribute.code',
                                '65' => 'attribute.type',
                                '66' => 'attribute.code',
                                '67' => 'attribute.type',
                                '68' => 'attribute.code',
                                '69' => 'attribute.type',
                                '70' => 'attribute.code',
                                '71' => 'attribute.type',
                                '72' => 'attribute.code',
                            ],
                            'product' => [
                                '73' => 'product.code',
                                '74' => 'product.lists.type',
                            ],
                            'property' => [
                                '75' => 'product.property.value',
                                '76' => 'product.property.type',
                            ],
                            'catalog' => [
                                '77' => 'catalog.code',
//                                '78' => 'catalog.lists.type',
                                '79' => 'catalog.code',
//                                '80' => 'catalog.lists.type',
                            ],
                        ],
	                    'location' => '/home/grajdanin/import_prod/',
//                        'skip-lines' => 1,
                    ],
                ],
                'export' => [
                    'sitemap' => [
                        'location' => '/var/www/kirmanshop.loc/current/public/',
                        'baseurl' => 'https://malyshkin.by',
                    ],
                ],
            ],
        ],
        'frontend' => [
            'catalog' => [
                'levels-always' => 2,
            ],
        ],
	],

	'i18n' => [
	    'ru' => [
	        'country' => [
                'BY' => ['Беларусь'],
            ],
            'states' => [
                'GM' => ['Гомельская'],
                'MG' => ['Могилевская'],
                'MS' => ['Минская'],
                'GD' => ['Гродненская'],
                'VT' => ['Витебская'],
                'BT' => ['Брестская'],
            ],
	        'client' => [
	            'Check' => ['исправить'],
                'Dear %1$s %2$s' => ['Уважаемый(ая) %1$s %2$s'],
	            'Login' => ['Вход'],
                'add' => ['Добавить'],
                'Your comment' => ['Ваш комментарий'],
                'Price' => ['Цена'],
                'Your reference' => ['Ваша ссылка'],
                'Our products' => ['Наши товары'],
                'New delivery address' => ['Новый адрес доставки'],
                'GM' => ['Гомельская'],
                'MG' => ['Могилевская'],
                'MS' => ['Минская'],
                'GD' => ['Гродненская'],
                'VT' => ['Витебская'],
                'BT' => ['Брестская'],
                'Street' => ['Адрес'],
                'Create a customer account for me' => ['Создать аккаунт для меня'],
                'Buy now' => ['Заказать'],
                'Thank you for your order and authorizing the payment.
An e-mail with the order details will be sent to you within the next few minutes.' => [
                    "Благодарим Вас за заказ.\nПисьмо с деталями заказа будет выслано Вам в течение нескольких минут."
                ],
            ],
            'client/code' => [
                'Ustanovka bloka vpered ili nazad' => ['Установка блока вперед и назад'],
                'Svetootrazhayuschie nashivki (fl' => ['Световозвращающие нашивки (фликеры)'],
                'Reguliruemyij naklon podgolovnik' => ['Регулируемый наклон подголовника'],
                'Regulirovka naklona spinki v lyu' => ['Регулировка наклона спинки'],
                'Regulirovanie vyisotyi podgolovn' => ['Регулирование высоты подголовника'],
                'Regulirovka naklona spinki kresl' => ['Регулировка наклона спинки кресла'],
            ],
        ],
	],


	'madmin' => [
		'cache' => [
			'manager' => [
				// 'name' => 'None', // Disable caching for development
			],
		],
		'log' => [
			'manager' => [
				'standard' => [
					'loglevel' => 7, // Enable debug logging into madmin_log table
				],
			],
		],
	],

	'mshop' => [
	],


	'command' => [
	],

	'frontend' => [
	],

	'backend' => [
	],

];
