@extends('shop::base')

@section('content')
    <!-- Breadcrumbs Start -->
    <section class="breadcrumb-bg margin-bottom-80">
        <span class="gray-color-mask color-mask"></span>
        <div class="theme-container container">
            <div class="site-breadcrumb relative-block space-75">
                <h2 class="section-title">
                    <span>
                        <span class="funky-font blue-tag">О нас</span>
                        <span class="italic-font"></span>
                    </span>
                </h2>
                <h3 class="sub-title"></h3>
                <hr class="dash-divider">
                <ol class="breadcrumb breadcrumb-menubar">
                    <li><a href="/shop"> За покупками </a>  /  <span class="blue-color">О нас</span> </li>
                </ol>
            </div>
        </div>
    </section>
    <!-- / Breadcrumbs Ends -->
    <div class="col-md-3"></div>
    <div class="col-md-6">
{{--        <div class="about-detail space-bottom-45">--}}

{{--            <p>Режим работы: Работаем с 10:00 до 20:00 каждый день.</p>--}}
{{--            <h3>Доставка товаров</h3><a name="return"></a>--}}
{{--            <p>Бесплатная доставка осуществляется по всей Гомельской области курьером (либо почтой - зависит от количества заказов и габаритов).</p>--}}
{{--            <p>При доставке курьером, товар доставляется до двери и при необходимоти курьер проконсультирует вас .</p>--}}
{{--            <p>В день доставки с вами свяжется курьер и обсудит время и место доставки.</p>--}}
{{--            <p>В случае отказа от товара в момент его фактического получения, покупатель оплачивает транспортные расходы в размере 10руб.(Речица - 5руб.), согласно ст.467 п.3 Гражданского кодекса РБ.</p>--}}
{{--            <h3>Возврат и обмен</h3>--}}
{{--            <ul>--}}
{{--                <li>При возврате товара необходимо соблюдать следующее:</li>--}}
{{--                <li><i class="fa fa-angle-right pink-color"></i> товар не должен быть использован;</li>--}}
{{--                <li><i class="fa fa-angle-right pink-color"></i> должна сохранится заводская упаковка (иная при наличии);</li>--}}
{{--                <li><i class="fa fa-angle-right pink-color"></i> должен сохранится гарантийный талон, инструкция или другие документы, сопровождающие товар.</li>--}}
{{--            </ul>--}}
{{--            <p>В случае возврата товара, покупатель оплачивает транспортные расходы в размере 10руб.(Речица - 5руб.), согласно ст.467 п.3 Гражданского кодекса РБ.</p>--}}
{{--            <p>Возврат товара и обмен недоброкачественного товара подлежит в течении 14 дней.</p>--}}
{{--        </div>--}}
        <div class="about-detail space-bottom-45">
            <a name="info"></a>
            <h3>Инфо</h3>
            <p>Индивидуальный предприниматель Кирман Алина Юрьевна проживающая по адресу:</p>
            <p>Гомельская область, Речицкий район, г.Речица, ул. Ивановская, 58-12.</p>
            <p>УНП 491515640. Свидетельство о государственной регистрации выдано 11.05.2020г.
                Речицким районным исполнительным комитетом.</p>
            <p>Внесён в  Торговый реестр РБ 16.10.2020 г.</p>
            <h3>Режим работы интернет-магазина Malyshkin.by</h3>
            <p>Пн-Пт 8.00-20.00</p>
            <p>Сб-Вс 10.00-20.00</p>
            <p>Приём заказов через корзину сайта осуществляется круглосуточно.</p>
            <p>В праздничные дни заказы принимаются только через корзину интернет-магазина.</p>
            <h3>Оформление заказа</h3>
            <ul>
                <li><i class="fa fa-angle-right pink-color"></i> Заказ оформляется через сайт и/или по телефону</li>
                <li><i class="fa fa-angle-right pink-color"></i> При оформлении заказа через сайт покупателем заполняется электронная форма заказа на товар и отправляется продавцу посредством сети Интернет.</li>
                <a name="delivery"></a>
                <li><i class="fa fa-angle-right pink-color"></i> при оформлении заказа по телефону покупатель предоставляет продавцу
                    информацию необходимую для оформления заказа: ФИО покупателя и получателя заказа,
                    адрес доставки заказа, контактный телефон.</li>
                <li><i class="fa fa-angle-right pink-color"></i> Информация полученная продавцом от покупателя необходима для уточнения
                    конкретной даты и времени доставки заказа.</li>
                <li><i class="fa fa-angle-right pink-color"></i> Если после получения заказа продавцом обнаруживается, что на складе отсутствует
                    необходимый товар или его количество, то продавец информирует об этом покупателя по
                    контактному телефону, отражённому в заказе. Покупатель имеет право согласится
                    принять товар в количестве, имеющемся в наличии у продавца, либо отказаться от заказа.</li>
                <li><i class="fa fa-angle-right pink-color"></i> При возникновении у покупателя вопросов, касающихся описания, свойств и
                    характеристик товара, перед оформлением заказа покупатель должен обратится к
                    продавцу по номеру телефона, указанному в разделе «Контакты».</li>
            </ul>
            <h3>Доставка</h3>
            <p>Мы осуществляем бесплатную доставку по г. Гомелю и г. Речица (с возможностью
                доставки за город в пределах 10 км).</p>
            <a name="payment"></a>
            <p>Г. Гомель – Ср. (19.00-22.00), Сб.(10.00-22.00)</p>
            <p>Г.Речица – Пн.-Пт.(18.00-22.00), Сб-Вс (10.00-22.00)</p>
            В праздничные дни доставка осуществляется по договорённости.
            <h4>Правила доставки</h4>
            <p>В момент передачи заказа покупатель проверяет целостность и комплектность заказа.</p>
            <p>При доставке Вам будут переданы все необходимые документы на покупку: инструкция
            по эксплуатации (паспорт на изделие), гарантийный талон (если это предусмотрено
                заводом изготовителем), товарный и кассовый чеки.</p>
            <a name="return"></a>
            <p>Согласно Постановления Совета Министров Республики Беларусь №31 «Об утверждении
            Правил осуществления розничной торговли по образцам» от 15.01.2009 г. п.16 покупатель
            вправе отказаться от товара , по независящим от нашего интернет-магазина причинам,
                возместив понесённые расходы, связанные с доставкой.</p>
            <h3>Способы оплаты</h3>
            <p>На данный момент в нашем интернет-магазине доступна оплата наличными и
                банковскими картами во время доставки Вашего заказа.</p>
            <a name="cert"></a>
            <h3>Возврат и гарантии</h3>
            <p>Возврат производится согласно Закона Республики Беларусь «О защите прав
                потребителей» от 09.01.2002 г. №90-3.</p>
            <p>Гарантийное и послегарантийное обслуживание проданных товаров осуществляется
            организациями-изготовителями или сервисными центрами, указанными в прилагаемых
                документах к товару при покупке.</p>
            <h3>Сертификация</h3>
            <p>Товары в нашем магазине имеют все необходимые сертификаты качества и
                безопасности для каждой категории товаров.</p>
        </div>
    </div>
    <div class="col-md-3"></div>
@endsection
