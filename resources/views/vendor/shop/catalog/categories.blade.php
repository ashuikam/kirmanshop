@section('categories')
<?php
$branch = $categories->getChildren();
?>

<article class="header-navigation">
    <nav class="navbar navbar-default product-menu">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#product-menu" >
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse no-padding" id="product-menu">
            <ul class="nav navbar-nav">
            @foreach ($branch as $item )
                <?php
                $id = $item->getId();
                $name = $item->getName();
                $uriname = str_replace(' ', '_', $name);
                $cbranch = $item->getChildren();
                ?>
                @if(count($cbranch) > 0)
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="" role="button" aria-haspopup="true" href="<?= '/shop/' . urlencode($uriname . '~' . $id) ?>">
                        <?=$name?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu ">
                        @foreach($cbranch as $citem)
                            <?php
                            $cid = $citem->getId();
                            $cname = $citem->getName();
                            $curiname = str_replace(' ', '_', $cname);
                            ?>
                        <li>
                            <a href="<?= '/shop/' . urlencode($curiname . '~' . $cid) ?>"><?=$cname?></a>
                        </li>
                        @endforeach
                    </ul>
                </li>
                @else
                <li><a href="<?= '/shop/' . urlencode($uriname . '~' . $id) ?>"><?=$name?></a></li>
                @endif
            @endforeach
            </ul>
        </div>
    </nav>
</article>
@stop
