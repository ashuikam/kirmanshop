
@section('search')
    <div class="header-search col-md-6  col-sm-5">
        <form action="/shop" class="search-form">
            <div class="no-padding search-cat">
                <label>
                    <span class="screen-reader-text">Искать</span>
                    <input type="search" title="Искать"
                           name="f_search" value=""
                           class="search-field"
                           data-url="/shop/suggest"
                            autocomplete="off">
                </label>
                <input type="submit" value="Search" class="search-submit">
            </div>
        </form>
    </div>
@stop
