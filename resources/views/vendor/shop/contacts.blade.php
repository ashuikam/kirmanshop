@extends('shop::base')

@section('content')
    <!-- Breadcrumbs Start -->
    <section class="breadcrumb-bg margin-bottom-80">
        <span class="gray-color-mask color-mask"></span>
        <div class="theme-container container">
            <div class="site-breadcrumb relative-block space-75">
                <h2 class="section-title">
                            <span>
                                <span class="funky-font blue-tag">Детальная информация</span>
                                <span class="italic-font"></span>
                            </span>
                </h2>
                <h3 class="sub-title"></h3>
                <hr class="dash-divider">
                <ol class="breadcrumb breadcrumb-menubar">
                    <li><a href="/shop"> За покупками </a>  /  <span class="blue-color">Наши контакты</span> </li>
                </ol>
            </div>
        </div>
    </section>
    <!-- / Breadcrumbs Ends -->
    <section class="col-md-8 center-block">
        <div class="col-md-6">

        </div>
        <div class="col-md-4">
            <div class="title-wrap space-bottom-25">
                <h2 class="section-title">
                <span>
                    <span class="funky-font blue-tag">Наши Контакты</span>
                </span>
                </h2>
            </div>
            <div class="contact-details">
                <ul>
                    <li>
                        <i class="fa fa-phone pink-color"></i>
                        <div class="details">
                            <span><strong class="pink-color"> телефон: </strong></span>
                            <span> +375447346448 </span>
                        </div>
                    </li>
                    <li>
                        <i class="fa fa-phone pink-color"></i>
                        <div class="details">
                            <span><strong class="pink-color"> телефон: </strong></span>
                            <span> +375447346447 </span>
                        </div>
                    </li>
                    <li>
                        <i class="fa fa-envelope green-color"></i>
                        <div class="details">
                            <span><strong class="green-color"> Почта: </strong></span>
                            <a href="mailto:manager@malyshkin.by"> <span> manager@malyshkin.by </span> </a>
                        </div>
                    </li>
                    <li>
                        <i class="fa  fa-globe golden-color"></i>
                        <div class="details">
                            <span><strong class="golden-color"> Вебсайт и Соцсети: </strong></span>
                            <a href="http://malyshkin.by"> <span> malyshkin.by </span> </a>
                            <ul class="social-icon">
                                <li>
                                    <a href="https://instagram.com/malyshkin.by?igshid=1dyncy9e132em">
                                        <i class="fa fa-instagram"> </i> <span class="instagram" >Instagram</span>
                                    </a>
                                </li>
{{--                                <li> <a href="#"> <i class="fa fa-facebook-square"></i> </a> </li>--}}
{{--                                <li> <a href="#"> <i class="fa fa-twitter-square"></i>  </a></li>--}}
{{--                                <li> <a href="#"> <i class="fa fa-pinterest-square"></i>  </a> </li>--}}
{{--                                <li> <a href="#"> <i class="fa fa-google-plus-square"></i>  </a> </li>--}}
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>

@endsection
