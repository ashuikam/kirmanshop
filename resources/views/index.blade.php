@extends('master')

@section('aimeos_styles')
    <link type="text/css" rel="stylesheet" href="{{ asset('packages/aimeos/shop/themes/elegance/aimeos.css') }}" />
@endsection

@section('own_styles')
    <link href="{{ asset('packages/aimeos/shop/themes/own/assets/css/fromaimeos.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('packages/aimeos/shop/themes/own/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('packages/aimeos/shop/themes/own/assets/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('packages/aimeos/shop/themes/own/assets/css/media.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('packages/aimeos/shop/themes/own/assets/plugins/owl-carousel/owl.carousel.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('packages/aimeos/shop/themes/own/assets/plugins/owl-carousel/owl.theme.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('packages/aimeos/shop/themes/own/assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('packages/aimeos/shop/themes/own/assets/plugins/jquery-ui-1.11.4.custom/jquery-ui.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('packages/aimeos/shop/themes/own/assets/css/subscribe-better.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('packages/aimeos/shop/themes/own/assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
@stop

@section('aimeos_scripts')
    <script type="text/javascript" src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>
    <script type="text/javascript" src="{{ asset('packages/aimeos/shop/themes/jquery-ui.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('packages/aimeos/shop/themes/aimeos.js') }}"></script>
    <script type="text/javascript" src="{{ asset('packages/aimeos/shop/themes/elegance/aimeos.js') }}"></script>
@endsection

@section('own_scripts')
    <!-- JS Global -->
    <script src="{{ asset('packages/aimeos/shop/themes/own/assets/plugins/jquery/jquery-1.11.3.min.js') }}"></script>
    <script src="{{ asset('packages/aimeos/shop/themes/own/assets/plugins/jquery-ui-1.11.4.custom/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('packages/aimeos/shop/themes/own/assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('packages/aimeos/shop/themes/own/assets/plugins/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('packages/aimeos/shop/themes/own/assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"></script>

    <script src="{{ asset('packages/aimeos/shop/themes/own/assets/js/jquery.subscribe-better.js') }}"></script>

    <!-- JS Page Level -->
    <script src="{{ asset('packages/aimeos/shop/themes/own/assets/js/moment-with-locales.min.js') }}"></script>
    <script src="{{ asset('packages/aimeos/shop/themes/own/assets/plugins/countdown/jquery.plugin.min.js') }}"></script>
    <script src="{{ asset('packages/aimeos/shop/themes/own/assets/plugins/countdown/jquery.countdown.min.js') }}"></script>
    <script src="{{ asset('packages/aimeos/shop/themes/own/assets/js/theme.js') }}"></script>

    <script type="text/javascript" src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>
    <script src="{{ asset('packages/aimeos/shop/themes/own/assets/js/own.js') }}"></script>
@endsection

@section('footer')
    <footer class="footer">
        <div class="bg2-with-mask space-35">
            <span class="black-mask color-mask"></span>
            <div class="container theme-container">
                <div class="row space-top-35">
                    <aside class="col-md-3 col-sm-6">
                        <div class="footer-widget space-bottom-35">
                            <h3 class="footer-widget-title"> <i class="fa fa-phone-square blue-color"></i>  Наши контакты </h3>
{{--                            <p class="textwidget"> Phasellus placerat rutrum tristique. In hacse platea dictumst.  </p>--}}
                            <div class="address">
                                <ul>
                                    <li> <i class="fa fa-phone blue-color"></i> <span> А1 +375447346448 </span> </li>
                                    <li> <i class="fa fa-phone blue-color"></i> <span> А1 +375447346447 </span> </li>
                                    <li> <i class="fa fa-envelope blue-color"></i> <a href="mailto:manager@malyshkin.by"> <span> manager@malyshkin.by </span> </a></li>
{{--                                    <li> <i class="fa fa-map-marker blue-color"></i> <span>121 King St </span> <span>Melbourne VIC 3000 Australi.</span> </li>--}}
                                </ul>
                            </div>
                            <div class="social-icon">
                                <ul>
                                    <li>
                                        <a href="https://instagram.com/malyshkin.by?igshid=1dyncy9e132em">
                                            <i class="fa fa-instagram"> </i> <span class="instagram" >Instagram</span>
                                        </a>
                                    </li>
{{--                                    <li> <a href="#"> <i class="fa fa-facebook-square"></i> </a> </li>--}}
{{--                                    <li> <a href="#"> <i class="fa fa-twitter-square"></i>  </a></li>--}}
{{--                                    <li> <a href="#"> <i class="fa fa-pinterest-square"></i>  </a> </li>--}}
{{--                                    <li> <a href="#"> <i class="fa fa-google-plus-square"></i>  </a> </li>--}}
                                </ul>
                            </div>
                        </div>
                    </aside>
                    <aside class="col-md-3 col-sm-6">
                        <div class="footer-widget space-bottom-35">
                            <h3 class="footer-widget-title"> <i class="fa fa-user pink-color"></i> Мой аккаунт  </h3>
                            <ul>
                                <li> <a href="/myaccount"> <i class="fa fa-angle-right pink-color"></i>  Персональная информация</a> </li>
{{--                                <li> <a href="#"> <i class="fa fa-angle-right pink-color"></i>  Personal Information</a></li>--}}
{{--                                <li> <a href="#"> <i class="fa fa-angle-right pink-color"></i>  Address</a> </li>--}}
{{--                                <li> <a href="#"> <i class="fa fa-angle-right pink-color"></i>  Discount</a> </li>--}}
{{--                                <li> <a href="#"> <i class="fa fa-angle-right pink-color"></i>  Order History</a> </li>--}}
{{--                                <li> <a href="#"> <i class="fa fa-angle-right pink-color"></i>  My Credit Slip </a> </li>--}}
                            </ul>
                        </div>
                    </aside>
{{--                    <aside class="col-md-3 col-sm-6">--}}
{{--                        <div class="footer-widget space-bottom-35">--}}
{{--                            <h3 class="footer-widget-title"> <i class="fa fa-cog green-color"></i> Наш сервис </h3>--}}
{{--                            <ul>--}}
{{--                                <li> <a href="#"> <i class="fa fa-angle-right green-color"></i> Shipping & Return</a> </li>--}}
{{--                                <li> <a href="#"> <i class="fa fa-angle-right green-color"></i> International Shipping </a></li>--}}
{{--                                <li> <a href="#"> <i class="fa fa-angle-right green-color"></i> Secure Shopping </a> </li>--}}
{{--                                <li> <a href="#"> <i class="fa fa-angle-right green-color"></i> Affiliates  </a> </li>--}}
{{--                                <li> <a href="#"> <i class="fa fa-angle-right green-color"></i> Careers </a> </li>--}}
{{--                                <li> <a href="#"> <i class="fa fa-angle-right green-color"></i> FAQ </a> </li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </aside>--}}
                    <aside class="col-md-3 col-sm-6">
                        <div class="footer-widget space-bottom-35">
                            <h3 class="footer-widget-title"> <i class="fa fa-info-circle golden-color"></i> Информация </h3>
                            <div class="recent-post">
                                <ul>
                                    <li> <a href="/about"> <i class="fa fa-angle-right golden-color"></i> О нас</a> </li>
                                    <li> <a href="/about#delivery"> <i class="fa fa-angle-right golden-color"></i> О доставке </a></li>
                                    <li> <a href="/about#return"> <i class="fa fa-angle-right golden-color"></i> Возврат и обмен </a> </li>
{{--                                    <li> <a href="#"> <i class="fa fa-angle-right golden-color"></i> Правила и соглашения  </a> </li>--}}
{{--                                    <li> <a href="#"> <i class="fa fa-angle-right golden-color"></i> Manufactures </a> </li>--}}
{{--                                    <li> <a href="#"> <i class="fa fa-angle-right golden-color"></i> Suppliers </a> </li>--}}
                                </ul>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
        <div class="bg2-with-mask space-20 footer-meta">
            <span class="black-mask color-mask"></span>
            <div class="container theme-container">
                <div class="row">
                    <aside class="col-md-6 col-sm-6 copy-rights">
                        <p> © Copyright 2020 by <a href="#" class="blue-color"> malyshkin.by </a> -  Made by
                            <span class="green-color">
                                <a href="mailto:ashuikam@gmail.com">Mikalai Ashuika</a>
                            </span>
                        </p>
                    </aside>
                    <aside class="col-md-6 col-sm-6 payment-options">
                        <ul>
{{--                            <li> <a href="#"> <i class="fa fa-cc "></i> </a> </li>--}}
                            <li> <a href="#"> <i class="fa fa-cc-visa"></i> </a> </li>
                            <li> <a href="#"> <i class="fa fa-cc-mastercard"></i> </a> </li>
{{--                            <li> <a href="#"> <i class="fa fa-cc-paypal"></i> </a> </li>--}}
{{--                            <li> <a href="#"> <i class="fa fa-cc-stripe"></i> </a> </li>--}}
{{--                            <li> <a href="#"> <i class="fa fa-cc-amex"></i> </a> </li>--}}
{{--                            <li> <a href="#"> <i class="fa fa-cc-discover"></i> </a> </li>--}}
                        </ul>
                    </aside>
                </div>
            </div>
        </div>

    </footer>
@endsection

@section('slider')
    <section id="testimonials-slider" class="space-top-35">
        <div class="bg2-with-mask space-35">
            <span class="blue-color-mask color-mask"></span>
            <div class="container theme-container">
                <div class="testimonials-wrap space-35">
                    <div class="testimonials-slider">
                        <div class="item">
                            <div class="row">
                                <div class="testimonials-img col-md-1 col-sm-2">
                                    <a class="" href="#"><img  src="assets/img/partners/testimonials.png" alt=" "> </a>
                                </div>
                                <div class="testimonials-content col-md-10 col-sm-8">
                                    <p class="italic-font"> Хорошая консультация, заказала 3 прогулочных коляски - привезли бесплатно!!!! все показали !!! выбрала одну Turan Tutek 24s :)) короче 10 из 10</p>
                                    <h4>Елена</h4>
                                    <!--a class="italic-font" href="">http://themeforest.net/user/jthemes</a-->
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="testimonials-img col-md-1 col-sm-2">
                                    <a class="" href="#"><img  src="assets/img/partners/testimonials.png" alt=" "> </a>

                                </div>
                                <div class="testimonials-content col-md-10 col-sm-8">
                                    <p class="italic-font">Заказывал коляску для 2-х малышей. Хотели c женой брать Bart-Plast Terra, нас переубедили, заказали RAY Ultra Duo, ОЧЕНЬ ДОВОЛЬНЫ))) Настоящие профессионалы, все быстро и очень круто!!!! Отдельное СПАСИБО от моей жены очень удобно!!! )))))</p>
                                    <h4>Виктор</h4>
                                    <!--a class="italic-font" href="">http://themeforest.net/user/jthemes</a-->
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="testimonials-img col-md-1 col-sm-2">
                                    <a class="" href="#"><img  src="assets/img/partners/testimonials.png" alt=" "> </a>
                                </div>
                                <div class="testimonials-content col-md-10 col-sm-8">
                                    <p class="italic-font">Отличный сервис!!! сдала древний !!!тяжелый!!! трансформер получила крутую прогулку. 10 из 10</p>
                                    <h4>Вика</h4>
                                    <!--a class="italic-font" href="">http://themeforest.net/user/jthemes</a-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="testimonials-slider-links">
                        <span class="prev slider-btn"  data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </span>
                        <span class="next slider-btn"  data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <img src="{{ asset('packages/aimeos/shop/themes/own/assets/img/pattern/ziz-zag.png') }}" class="blue-zig-zag" alt=" ">
    </section>
@endsection

@section('main_slider')
<div class="container">
    <!-- Main Slider Start -->
    <section id="main-slider" class="carousel slide main-slider style-2 light-bg" >
        <div class="carousel-inner slider">
            <div class="item active">
                <img src="{{ asset('packages/aimeos/shop/themes/own/assets/img/slider/slide-1.jpg') }}" alt="...">
                <div class="theme-container container">
                    <div class="caption-text style-2">
                        <div class="title-wrap">
                            <h2 class="section-title">
                                <span >
                                    <span class="funky-font green-color green-tag">Скидка </span>
                                    <span class="italic-font"></span>
                                </span>
                            </h2>
                        </div>
                        <div class="discount-wrap">
                            <h2 class="sub-title">
                                <span class="golden-color"></span>
                                <span class="blue-color"></span>
                                <span class="golden-color">К открытию магазина</span>
                            </h2>
                            <ul class="discount-list">
                                <li class="funky-font pink-color"> На все товары!!! </li>
                            </ul>
                            <hr class="dash-divider">
                        </div>
                        <div class="slider-link">
                            <p> <strong class="golden-color discount funky-font">5%</strong></p>
                            <a class="green-btn btn" href="/shop"> За покупками! <i class="fa fa-caret-right"></i> </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="{{ asset('packages/aimeos/shop/themes/own/assets/img/slider/slide-5.png') }}" alt="...">
                <div class="theme-container container">
                    <div class="caption-text style-2">
                        <div class="title-wrap">
                            <h2 class="section-title">
                                <span >
                                    <span class="funky-font blue-color">Большой выбор</span>
                                    <span class="funky-font golden-color"></span>
                                    <span class="funky-font blue-color blue-tag"> колясок </span>
                                    <span class="italic-font golden-color">для малышей</span>
                                </span>
                            </h2>
                        </div>
                        <div class="discount-wrap">
{{--                            <h2 class="sub-title">Stylish<span class="green-color">  Handpicked  </span>Items</h2>--}}
{{--                            <ul class="discount-list">--}}
{{--                                <li> for baby & kids (0 - 12 Year) </li>--}}
{{--                            </ul>--}}
                            <hr class="dash-divider">
                        </div>
                        <div class="slider-link">
                            <p> Универсальные <strong class="blue-color">Прогулочные</strong> Классика </p>
                            <a class="blue-btn btn" href="/shop/Коляски~2"> За покупками! <i class="fa fa-caret-right"></i> </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="{{ asset('packages/aimeos/shop/themes/own/assets/img/slider/slide-2.jpg') }}" alt="...">
                <div class="theme-container container">
                    <div class="caption-text style-2">
                        <div class="title-wrap">
                            <h2 class="section-title">
                                <span >
                                    <span class="funky-font green-color green-tag">Счастливый малыш </span>
                                    <span class="italic-font pink-color">Всё, что ему необходимо</span>
                                    <span class="italic-font pink-color">в нашем магазине</span>
                                </span>
                            </h2>
                        </div>
                        <div class="discount-wrap">
{{--                            <h2 class="sub-title">Stylish<span class="green-color">  Handpicked  </span>Items</h2>--}}
                            <ul class="discount-list">
{{--                                <li> for baby & kids (0 - 12 Year) </li>--}}
                            </ul>
                            <hr class="dash-divider">
                        </div>
                        <div class="slider-link">
{{--                            <p> Order More Than $150 Get Upto <strong class="green-color">40%</strong> Off </p>--}}
                            <a class="pink-btn btn" href="/shop"> За покупками! <i class="fa fa-caret-right"></i> </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Carousel Slide Button Start-->
        <div class="slider-btn-stle-2">
            <a class="left carousel-control slider-btn-2" href="#main-slider" data-slide="prev">
                <i class="fa fa-angle-left"></i>
            </a>
            <a class="right carousel-control slider-btn-2" href="#main-slider" data-slide="next">
                <i class="fa fa-angle-right"></i>
            </a>
        </div>
    </section>
    <!-- / Main Slider Ends -->
</div>
@endsection

@section('up')
    <div id="to-top" class="to-top"><i class="fa fa-angle-up"></i></div>
@endsection

@section('shipping')
    <!-- Newsletter Start -->
    <section id="news-letter" class="space-35">
        <div class="bg3-with-mask  space-top-35  news-letter">
            <span class="black-mask color-mask"></span>
            <div class="container theme-container">
                <div class="row">
{{--                    <div class="col-md-6 col-sm-6 space-35">--}}
{{--                        <div class="title-wrap space-bottom-45">--}}
{{--                            <h2 class="section-title">--}}
{{--                                <span>--}}
{{--                                    <span class="funky-font blue-tag">Newsletter </span>--}}
{{--                                    <span class="italic-font">Sign Up</span>--}}
{{--                                </span>--}}
{{--                            </h2>--}}
{{--                        </div>--}}
{{--                        <div class="newsletter-form">--}}
{{--                            <form class="newsletter">--}}
{{--                                <div class="form-group col-sm-8 no-padding">--}}
{{--                                    <label class="sr-only">Enter Child Name</label>--}}
{{--                                    <input type="text" placeholder="Enter your e-mail" class="form-control">--}}
{{--                                </div>--}}
{{--                                <div class="form-group col-sm-3 no-padding">--}}
{{--                                    <button class="blue-btn submit-btn btn" type="submit">Submit</button>--}}
{{--                                </div>--}}
{{--                            </form>--}}
{{--                            <p class="col-sm-9 no-padding">Curabitur sit amet mi non justo blandit rhoncus in porttitor diam. In libero in libero ultricies scelerisque.</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="col-md-4 col-sm-4 space-35">
                        <div class="title-wrap">
                            <h2 class="section-title">
                                <i class="fa fa-credit-card pink-color"></i>
                                <span>
                                    <span class="funky-font-shipping pink-tag">Гибкий расчет</span>
                                </span>
                            </h2>
                        </div>
                        <div class="text-widget">
                            <p>Вы можете расплатиться как банковской картой так и наличными!!!</p>
                            <a href="/about#payment" class="pink-color title-link"> Узнать больше <i class="fa fa-caret-right"></i> </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 space-35">
                        <div class="title-wrap">
                            <h2 class="section-title">
                                <i class="fa fa-truck green-color"></i>
                                <span>
                                    <span class="funky-font-shipping green-tag">Бесплатная доставка</span>
                                </span>
                            </h2>
                        </div>

                        <div class="text-widget">
                            <p>Вне зависимости от стоимости товара!!!</p>
                            <a href="/about#delivery" class="green-color title-link"> Узнать больше <i class="fa fa-caret-right"></i> </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 space-35">
                        <div class="title-wrap">
                            <h2 class="section-title">
                                <i class="fa fa-certificate golden-color"></i>
                                <span>
                                   <span class="funky-font-shipping golden-tag">Товар сертифицирован</span>
                                </span>
                            </h2>
                        </div>
                        <div class="text-widget">
                            <p>Все товары имеют сертификат качества РБ!!!</p>
                            <a href="/about#cert" class="golden-color title-link"> Узнать больше <i class="fa fa-caret-right"></i> </a>
                        </div>
                    </div>
{{--                    <div class="col-md-4 col-sm-4 space-35">--}}
{{--                        <div class="title-wrap">--}}
{{--                            <h2 class="section-title">--}}
{{--                                <i class="fa fa-reply pink-color"></i>--}}
{{--                                <span>--}}
{{--                                   <span class="funky-font-shipping green-tag">Возврат</span>--}}
{{--                                </span>--}}
{{--                            </h2>--}}
{{--                        </div>--}}
{{--                        <div class="text-widget">--}}
{{--                            <p>Все товары имет сертификат качества РБ!!!</p>--}}
{{--                            <a href="/about" class="pink-color title-link"> Узнать больше <i class="fa fa-caret-right"></i> </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </section>
    <!-- / Newsletter Ends -->
@endsection

@section('main-welcome')
    <section class="page-section space-bottom-45">
        <div class="title-wrap with-border space-bottom-35">
            <h2 class="section-title with-border">
                <span class="white-bg">
                    <span class="funky-font">Мы рады видеть вас в нашем магазине!</span>
                </span>
            </h2>
        </div>
        <div class="container">
            <div class="row">
                <div class="flex-box">
                    <div class="col-md-6 no-padding">
                        <img class="default-box-shadow light-bg-reverse" src="{{ asset('packages/aimeos/shop/themes/own/assets/img/slider/welcome-1.png') }}">
                    </div>
                    <div class="col-md-6 no-padding">
                        <div class="default-box-shadow light-bg-reverse">
                            <p>Каждые родители хотят выбрать для своего ребенка самое лучшее. Мы поможем Вам! Ведь мы тоже родители. </p>
                            <p>Нам будет приятно доставить выбранный вами товар прямо к вашей двери и ответить на все интересующие вас вопросы совершенно бесплатно.</p>
                            <p>У нас вы найдёте только качественный и сертифицированный товар по доступным ценам для каждого клиента. <p>
                            <p>Мы будем оставаться с вами на связи, если вам будет необходимо сервисное обслуживание или дальнейшее консультирование при эксплуатации купленной вами продукции.</p>
                            <p>Нам доставит удовольствие принести в ваш дом ещё больше хороших впечатлений.</p>
                            <a class="pink-btn btn" href="/shop"> За покупками! <i class="fa fa-caret-right"></i> </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
