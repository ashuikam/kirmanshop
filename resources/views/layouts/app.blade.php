@extends('shop::base')


@section('login_header')
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

@endsection
@section('login')
    <div id="app">
        <main class="py-4">
            @yield('content')
        </main>
    </div>
@endsection

