@extends('shop::base')

@section('content')
    <section id="error-page" class="error-wrap light-bg space-80">
        <div class="theme-container container">
            <div class="row space-40">
                <div class="col-md-6 col-sm-5 text-right">
                    <h1 class="error-title funky-font">
                        <span class="blue-color">o</span><span class="pink-color">0</span><span class="green-color">p</span><span class="golden-color">s</span><span class="purple-color">!</span>
                    </h1>
                </div>
                <div class="col-md-6 col-sm-7 error-info">
                    <div class="title-wrap">
                        <h2 class="section-title">
                            <span>
                                <span class="funky-font blue-tag">Мы не можем</span>
                                <span class="italic-font">Найти страницу, которую вы запрашивали </span>
                            </span>
                        </h2>
                        <h3 class="sub-title">@yield('code') :<span class="blue-color"> Страница @yield('message') </span></h3>
                        <hr class="dash-divider">
                    </div>
                    <div class="error-details">
                        <p>

                        </p>
                        <a class="blue-btn btn" href="/shop"><i class="fa fa-caret-left"></i> Вернуться за покупками </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
