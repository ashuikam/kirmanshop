@extends('errors::minimal')

@section('title', __('Не найдена'))
@section('code', '404')
@section('message', __('Не найдена'))
