@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4"><!-- baner --></div>
        <div class="col-md-6">
            <div class="login-wrap">
                <h2>{{ __('Сброс пароля') }}</h2>
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <form method="POST" action="{{ route('password.email') }}">
                    @csrf

                    <div class="form-group">
                        <input id="email"
                               type="email"
                               class="form-control @error('email') is-invalid @enderror"
                               name="email" value="{{ old('email') }}"
                               required autocomplete="email"
                               placeholder="Введите E-mail"
                        >
                        <i class="blue-color fa fa-user"></i>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <button type="submit" class="blue-btn btn">
                            {{ __('Выслать ссылку для сброса пароля') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-4"><!-- baner --></div>
    </div>
</div>
@endsection
