@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4"><!-- baner --></div>
        <div class="col-md-6">
            <h2>{{ __('Сброс пароля') }}</h2>

            <div class="login-wrap">
                <form method="POST" action="{{ route('password.update') }}">
                    @csrf

                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="form-group">
                        <input id="email"
                               type="email"
                               class="form-control @error('email') is-invalid @enderror"
                               name="email"
                               value="{{ $email ?? old('email') }}"
                               required autocomplete="email"
                               placeholder="Введите E-mail"
                        >
                        <i class="blue-color fa fa-user"></i>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <input id="password"
                               type="password"
                               class="form-control @error('password') is-invalid @enderror"
                               name="password"
                               required autocomplete="new-password"
                               placeholder="Введите новый пароль"
                        >
                        <i class="pink-color fa  fa-unlock-alt"></i>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <input id="password-confirm"
                               type="password"
                               class="form-control"
                               name="password_confirmation"
                               required autocomplete="new-password"
                               placeholder="Подтвердите новый пароль"
                        >
                        <i class="pink-color fa  fa-unlock-alt"></i>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="blue-btn btn">
                            {{ __('Сбросить пароль') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-4"><!-- baner --></div>
    </div>
</div>
@endsection
