@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4"><!-- banner --></div>
        <div class="col-md-6">
            <h2>{{ __('Вход') }}</h2>

            <div class="login-wrap">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group">
                        <input id="email"
                               type="email"
                               class="form-control @error('email') is-invalid @enderror"
                               name="email" value="{{ old('email') }}"
                               required autocomplete="email"
                               placeholder="Введите E-mail"
                        >
                        <i class="blue-color fa fa-user"></i>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <input id="password"
                               type="password"
                               class="form-control @error('password') is-invalid @enderror"
                               name="password"
                               required autocomplete="current-password"
                               placeholder="Введите пароль"
                        >
                        <i class="pink-color fa  fa-unlock-alt"></i>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input"
                                   type="checkbox"
                                   name="remember"
                                   id="remember" {{ old('remember') ? 'checked' : '' }}
                            >

                            <label class="form-check-label" for="remember">
                                {{ __('Запомнить меня') }}
                            </label>
                        </div>
                        <label class="forgot-pwd">
                            @if (Route::has('password.request'))
                                <a class="blue-color title-link" href="{{ route('password.request') }}">
                                    {{ __('Забыли пароль?') }}
                                </a>
                            @endif
                        </label>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="blue-btn btn">
                            {{ __('Войти') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-4"><!-- banner --></div>
</div>
@endsection
