@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4"><!-- banner --></div>
        <div class="col-md-6">
            <h2>{{ __('Регистрация') }}</h2>
            <div class="login-wrap">
                <form method="POST" action="{{ route('register') }}">
                    @csrf

                    <div class="form-group">
                        <input id="name"
                               type="text"
                               class="form-control @error('name') is-invalid @enderror"
                               name="name"
                               value="{{ old('name') }}"
                               required autocomplete="name"
                               placeholder="Имя"
                        >
                        <i class="blue-color fa fa-user"></i>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <input id="email"
                               type="email"
                               class="form-control @error('email') is-invalid @enderror"
                               name="email" value="{{ old('email') }}"
                               required autocomplete="email"
                               placeholder="Введите E-mail"
                        >
                        <i class="blue-color fa fa-user"></i>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <input id="password"
                               type="password"
                               class="form-control @error('password') is-invalid @enderror"
                               name="password"
                               required autocomplete="new-password"
                               placeholder="Введите пароль"
                        >
                        <i class="pink-color fa  fa-unlock-alt"></i>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <input id="password-confirm"
                               type="password"
                               class="form-control"
                               name="password_confirmation"
                               required autocomplete="new-password"
                               placeholder="Подтвердите пароль"
                        >
                        <i class="pink-color fa  fa-unlock-alt"></i>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="blue-btn btn">
                            {{ __('Регистрация') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-4"><!-- banner --></div>
    </div>
</div>
@endsection


<div class="modal fade login-register" id="login-register" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        <div class="modal-content light-bg">
            <div class="col-sm-8">
                <div class="title-wrap">
                    <h2 class="section-title">
                                    <span>
                                        <span class="funky-font blue-tag">Welcome</span>
                                        <span class="italic-font">to the Baby & kids Store</span>
                                    </span>
                    </h2>
                    <h3 class="sub-title">Sign in Or Register</h3>
                    <hr class="dash-divider-small">
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse at purus eu eros maximus elementum sed eget erat.
                    esent in varius diam, sit amet ultricies nisi. Maecenas urna odio.</p>
            </div>
            <div class="col-sm-6 col-md-5">
                <div class="login-wrap">
                    <h2 class="title-2 sub-title-small">Sign In</h2>
                    <form>
                        <div class="form-group">
                            <input type="text" placeholder="Username or email" class="form-control" required />
                            <i class="blue-color fa fa-user"></i>
                        </div>
                        <div class="form-group">
                            <input type="password" placeholder="Password" class="form-control" required />
                            <i class="pink-color fa  fa-unlock-alt"></i>
                        </div>
                        <div class="form-group">
                            <label class="chk-box"><input type="checkbox" name="optradio">Keep me logged In</label>
                            <label class="forgot-pwd">
                                <a href="#" class="blue-color title-link">Forgot Password?</a>
                            </label>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="blue-btn btn">Login</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-6 col-md-5">
                <div class="register-wrap">
                    <h2 class="title-2 sub-title-small">New User Here?</h2>
                    <p class="italic-font">Registration is free and easy!</p>
                    <ul>
                        <li>Faster checkout</li>
                        <li>Save multiple shipping addresses</li>
                        <li>View and track orders and more</li>
                    </ul>
                    <a href="#" class="pink-btn btn"> Create An Account </a>
                </div>
            </div>
        </div>
    </div>
</div>
