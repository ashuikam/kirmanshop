<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    @yield('aimeos_header')
    @yield('login_header')
	<title>Malyshkin.by</title>

    @yield('own_styles')
	<link type="text/css" rel="stylesheet" href='https://fonts.googleapis.com/css?family=Roboto:400,300'>
	<!--link type="text/css" rel="stylesheet" href="/css/app.css"-->

    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>


</head>
<body id="home" class="wide">
    <!-- PRELOADER -->
    <div id="preloader">
        <div class='baby'>
            <div class='head'>
                <div class='eye'></div>
                <div class='cheek'></div>
                <div class='horn'></div>
            </div>
            <div class='back'>
                <div class='tail'></div>
                <div class='hand'></div>
                <div class='feet'></div>
                <div class='ass'></div>
            </div>
        </div>
    </div>
    <!-- /PRELOADER -->
    <main class="wrapper">
        <header class="light-bg">
            <!-- Header top bar starts-->
            <section class="top-bar">
                <div class="bg-with-mask box-shadow">
                    <span class="blue-color-mask color-mask"></span>
                    <div class="container theme-container">
                        <nav class="navbar navbar-default top-navbar">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" >
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="collapse navbar-collapse no-padding" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav pull-right">
                                    <li><a href="/contacts">Наши контакты</a></li>
                                    @if (Auth::guest())
                                        <li>
                                            <a class="nav-link" href="/login">
                                                Вход
                                            </a>
                                        </li>
                                        <li><a class="nav-link" href="/register">Регистрация</a></li>
                                    @else
                                        <li class="dropdown">
                                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="{{ route('aimeos_shop_account',['site'=>Route::current()->parameter('site','default'),'locale'=>Route::current()->parameter('locale','ru'),'currency'=>Route::current()->parameter('currency','BYN')]) }}" title="Profile">
                                                        Профиль
                                                    </a>
                                                </li>
                                                <li>
                                                    <form id="logout" action="/logout" method="POST">{{csrf_field()}}</form>
                                                    <a href="javascript: document.getElementById('logout').submit();">Выход</a>
                                                </li>
                                            </ul>
                                        </li>
                                @endif
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
                <img src="{{ asset('packages/aimeos/shop/themes/own/assets/img/pattern/ziz-zag.png') }}" class="blue-zig-zag" alt=" ">
            </section>
            <!-- /Header top bar ends -->
            <section class="header-wrapper white-bg fixed">
                <div class="row">
                    <div class="container theme-container ">
                        <article class="header-middle">
                            <!-- Logo -->
                            <div class="logo col-md-3  col-sm-3">
                                <a href="/"><img src="{{ asset('packages/aimeos/shop/themes/own/assets/img/logo/logo.png') }}" alt=" "/></a>
                            </div>
                            <!-- /Logo -->

                            <!-- Header search -->
                            @yield('search')
                            <!-- /Header search -->

                            <!-- Header shopping cart -->
                            @yield('aimeos_head')
                            <!-- Header shopping cart -->
                            </article>
                            @yield('categories')
                        </div>
                    </div>
                </section>
        </header>
    </main>
    @yield('aimeos_stage')
    <article  class="container theme-container">
        <section class="product-category">
            <div class="row">
                @yield('aimeos_nav')
	            @yield('aimeos_body')
            </div>
        </section>
    </article>
	@yield('aimeos_aside')
	@yield('content')
    @yield('slider')
    @yield('footer')
    @yield('up')
	<!-- Scripts -->
	<script type="text/javascript" src="/js/app.js"></script>
    @yield('own_scripts')
</body>
</html>
