/**
 * Aimeos related Javascript code
 *
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @copyright Metaways Infosystems GmbH, 2012
 * @copyright Aimeos (aimeos.org), 2014-2018
 */




/**
 * Basket related client actions
 */
AimeosBasketRelated = {

	/**
	 * Initializes the basket related actions
	 */
	init: function() {
	}
};



/**
 * Catalog filter actions
 */
AimeosCatalogFilter = {

	MIN_INPUT_LEN: 3,

	/**
	 * Autocompleter for quick search
	 */
	setupSearchAutocompletion: function() {

		var aimeosInputComplete = $(".catalog-filter-search .value");

		if(aimeosInputComplete.length) {
			aimeosInputComplete.autocomplete({
				minLength : AimeosCatalogFilter.MIN_INPUT_LEN,
				delay : 200,
				source : function(req, resp) {
					var nameTerm = {};
					nameTerm[aimeosInputComplete.attr("name")] = req.term;

					$.getJSON(aimeosInputComplete.data("url"), nameTerm, function(data) {
						resp(data);
					});
				},
				select : function(ev, ui) {
					aimeosInputComplete.val(ui.item.label);
					return false;
				}
			}).autocomplete("instance")._renderItem = function(ul, item) {
				return $(item.html).appendTo(ul);
			};
		}
	},


	/**
	 * Sets up the form checks
	 */
	setupFormChecks: function() {

		$(".catalog-filter form").on("submit", function(ev) {

			var result = true;
			var form = $(this);

			$("input.value", this).each(function() {

				var input = $(this);

				if(input.val() !== '' && input.val().length < AimeosCatalogFilter.MIN_INPUT_LEN) {

					if($(this).has(".search-hint").length === 0) {

						var node = $('<div class="search-hint">' + input.data("hint") + '</div>');
						$(".catalog-filter-search", form).after(node);

						var pos = node.position();
						node.css("left", pos.left).css("top", pos.top);
						node.delay(3000).fadeOut(1000, function() {
							node.remove();
						});
					}

					result = false;
				}
			});

			return result;
		});
	},


	/**
	 * Sets up the fade out of the catalog list
	 */
	setupListFadeout: function() {

		$(".catalog-filter-tree li.cat-item").on("click", function() {
			$(".catalog-list").fadeTo(1000, 0.5);
		});
	},


	/**
	 * Toggles the categories if hover isn't available
	 */
	setupCategoryToggle: function() {

		$(".catalog-filter-tree").on("click", "h2", function(ev) {
			$("> ul", ev.delegateTarget).slideToggle();
		});
	},


	/**
	 * Toggles the attribute filters if hover isn't available
	 */
	setupAttributeToggle: function() {

		$(".catalog-filter-attribute").on("click", "h2", function(ev) {
			$(".attribute-lists", ev.delegateTarget).slideToggle();
		});
	},


	/**
	 * Toggles the attribute filters if hover isn't available
	 */
	setupAttributeListsToggle: function() {

		$(".catalog-filter-attribute .attribute-lists .attr-list").hide();

		$(".catalog-filter-attribute fieldset").on("click", "legend", function(ev) {
			$(".attr-list", ev.delegateTarget).slideToggle();
		});
	},


	/**
	 * Hides the attribute filter if no products are available for
	 */
	setupAttributeListsEmtpy: function() {

		$(".catalog-filter-attribute .attribute-lists fieldset").hide();

		$(".catalog-filter-attribute .attribute-lists .attr-count").each(function(ev) {
			$(this).parents('fieldset').show();
		});
	},


	/**
	 * Submits the form when clicking on filter attribute names or counts
	 */
	setupAttributeItemSubmit: function() {

		$(".catalog-filter-attribute li.attr-item").on("click", ".attr-name, .attr-count", function(ev) {
			var input = $("input", ev.delegateTarget);
			input.prop("checked") ? input.prop("checked", false) : input.prop("checked", true);

			$(this).parents(".catalog-filter form").submit();
			$(".catalog-list").fadeTo(1000, 0.5);
		});
	},


	/**
	 * Toggles the supplier filters if hover isn't available
	 */
	setupSupplierToggle: function() {

		$(".catalog-filter-supplier").on("click", "h2", function(ev) {
			$(".supplier-lists", ev.delegateTarget).slideToggle();
		});
	},


	/**
	 * Submits the form when clicking on filter supplier names or counts
	 */
	setupSupplierItemSubmit: function() {

		$(".catalog-filter-supplier li.attr-item").on("click", ".attr-name, .attr-count", function(ev) {
			var input = $("input", ev.delegateTarget);
			input.prop("checked") ? input.prop("checked", false) : input.prop("checked", true);

			$(this).parents(".catalog-filter form").submit();
			$(".catalog-list").fadeTo(1000, 0.5);
		});
	},


	/**
	 * Registers events for the catalog filter search input reset
	 */
	setupSearchTextReset: function() {

		$(".catalog-filter-search").on("keyup", ".value", function(ev) {
			if ($(this).val() !== "") {
				$(".reset .symbol", ev.delegateTarget).css("visibility", "visible");
			} else {
				$(".reset .symbol", ev.delegateTarget).css("visibility", "hidden");
			}
		});

		$(".catalog-filter-search").on("click", ".reset", function(ev) {
			$(".symbol", this).css("visibility", "hidden");
			$(".value", ev.delegateTarget).val("");
			$(".value", ev.delegateTarget).focus();
			return false;
		});
	},


	/**
	 * Initialize the catalog filter actions
	 */
	init: function() {

		this.setupCategoryToggle();
		this.setupSupplierToggle();
		this.setupAttributeToggle();
		this.setupAttributeListsEmtpy();
		this.setupAttributeListsToggle();
		this.setupListFadeout();

		this.setupAttributeItemSubmit();
		this.setupSupplierItemSubmit();

		this.setupFormChecks();
		this.setupSearchTextReset();
		this.setupSearchAutocompletion();
	}
};

/**
 * Catalog session actions
 */
AimeosCatalogSession = {

	/**
	 * Initializes the catalog session actions
	 */
	init: function() {
	}
};

/**
 * Catalog stage actions
 */
AimeosCatalogStage = {

	/**
	 * Initializes the catalog stage actions
	 */
	init: function() {
	}
};

/**
 * Checkout confirm client actions
 */
AimeosCheckoutConfirm = {

	/**
	 * Initializes the checkout confirm section
	 */
	init: function() {
	}
};






jQuery(document).ready(function($) {

	/* Lazy product image loading in list view */
	// Aimeos.loadImages();
	// $(window).on("resize", Aimeos.loadImages);
	// $(window).on("scroll", Aimeos.loadImages);


	// Aimeos.init();

	// AimeosLocaleSelect.init();

	// AimeosCatalog.init();
	// AimeosCatalogFilter.init();
	// AimeosCatalogList.init();
	// AimeosCatalogSession.init();
	// AimeosCatalogStage.init();

	// AimeosBasketBulk.init();
	// AimeosBasketMini.init();
	// AimeosBasketRelated.init();
	// AimeosBasketStandard.init();
    //
	// AimeosCheckoutStandard.init();
	// AimeosCheckoutConfirm.init();
    //
	// AimeosAccountProfile.init();
	// AimeosAccountSubscription.init();
	// AimeosAccountHistory.init();
	// AimeosAccountFavorite.init();
	// AimeosAccountWatch.init();

});
