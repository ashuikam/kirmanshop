<?php

/**
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @copyright Aimeos (aimeos.org), 2016-2018
 */

/* Expected data:
 * - products : List of product items
 * - productItems : List of product variants incl. referenced items (optional)
 * - basket-add : True to display "add to basket" button, false if not (optional)
 * - require-stock : True if the stock level should be displayed (optional)
 * - itemprop : Schema.org property for the product items (optional)
 * - position : Position is product list to start from (optional)
 */

$enc = $this->encoder();
$position = $this->get( 'position' );
$productItems = $this->get( 'productItems', [] );

if( $this->get( 'basket-add', false ) )
{
    $basketTarget = $this->config( 'client/html/basket/standard/url/target' );
    $basketController = $this->config( 'client/html/basket/standard/url/controller', 'basket' );
    $basketAction = $this->config( 'client/html/basket/standard/url/action', 'index' );
    $basketConfig = $this->config( 'client/html/basket/standard/url/config', [] );
    $basketSite = $this->config( 'client/html/basket/standard/url/site' );

    $basketParams = ( $basketSite ? ['site' => $basketSite] : [] );
}


/** client/html/catalog/detail/url/target
 * Destination of the URL where the controller specified in the URL is known
 *
 * The destination can be a page ID like in a content management system or the
 * module of a software development framework. This "target" must contain or know
 * the controller that should be called by the generated URL.
 *
 * @param string Destination of the URL
 * @since 2014.03
 * @category Developer
 * @see client/html/catalog/detail/url/controller
 * @see client/html/catalog/detail/url/action
 * @see client/html/catalog/detail/url/config
 * @see client/html/catalog/detail/url/filter
 */
$detailTarget = $this->config( 'client/html/catalog/detail/url/target' );

/** client/html/catalog/detail/url/controller
 * Name of the controller whose action should be called
 *
 * In Model-View-Controller (MVC) applications, the controller contains the methods
 * that create parts of the output displayed in the generated HTML page. Controller
 * names are usually alpha-numeric.
 *
 * @param string Name of the controller
 * @since 2014.03
 * @category Developer
 * @see client/html/catalog/detail/url/target
 * @see client/html/catalog/detail/url/action
 * @see client/html/catalog/detail/url/config
 * @see client/html/catalog/detail/url/filter
 */
$detailController = $this->config( 'client/html/catalog/detail/url/controller', 'catalog' );

/** client/html/catalog/detail/url/action
 * Name of the action that should create the output
 *
 * In Model-View-Controller (MVC) applications, actions are the methods of a
 * controller that create parts of the output displayed in the generated HTML page.
 * Action names are usually alpha-numeric.
 *
 * @param string Name of the action
 * @since 2014.03
 * @category Developer
 * @see client/html/catalog/detail/url/target
 * @see client/html/catalog/detail/url/controller
 * @see client/html/catalog/detail/url/config
 * @see client/html/catalog/detail/url/filter
 */
$detailAction = $this->config( 'client/html/catalog/detail/url/action', 'detail' );

/** client/html/catalog/detail/url/config
 * Associative list of configuration options used for generating the URL
 *
 * You can specify additional options as key/value pairs used when generating
 * the URLs, like
 *
 *  client/html/<clientname>/url/config = array( 'absoluteUri' => true )
 *
 * The available key/value pairs depend on the application that embeds the e-commerce
 * framework. This is because the infrastructure of the application is used for
 * generating the URLs. The full list of available config options is referenced
 * in the "see also" section of this page.
 *
 * @param string Associative list of configuration options
 * @since 2014.03
 * @category Developer
 * @see client/html/catalog/detail/url/target
 * @see client/html/catalog/detail/url/controller
 * @see client/html/catalog/detail/url/action
 * @see client/html/catalog/detail/url/filter
 * @see client/html/url/config
 */
$detailConfig = $this->config( 'client/html/catalog/detail/url/config', [] );

/** client/html/catalog/detail/url/filter
 * Removes parameters for the detail page before generating the URL
 *
 * For SEO, it's nice to have product URLs which contains the product names only.
 * Usually, product names are unique so exactly one product is found when resolving
 * the product by its name. If two or more products share the same name, it's not
 * possible to refer to the correct product and in this case, the product ID is
 * required as unique identifier.
 *
 * This setting removes the listed parameters from the URLs of the detail pages.
 *
 * @param array List of parameter names to remove
 * @since 2019.04
 * @category User
 * @category Developer
 * @see client/html/catalog/detail/url/target
 * @see client/html/catalog/detail/url/controller
 * @see client/html/catalog/detail/url/action
 * @see client/html/catalog/detail/url/config
 */
$detailFilter = array_flip( $this->config( 'client/html/catalog/detail/url/filter', ['d_prodid'] ) );


?>
    <div id="best-sellers" class="" role="tabpanel">
        <div class="col-md-12 product-wrap default-box-shadow">
            <div class="title-wrap">
                <h2 class="section-title">
                    <span>
                        <span class="funky-font blue-tag"><?= $this->translate( 'client', 'Top seller' ); ?></span>
<!--                        <span class="italic-font">Sellers</span>-->
                    </span>
                </h2>
                <div class="poroduct-pagination">
                    <span class="product-slide blue-background next"> <i class="fa fa-chevron-left"></i> </span>
                    <span class="product-slide blue-background prev"> <i class="fa fa-chevron-right"></i> </span>
                </div>
            </div>
            <div class="product-slider owl-carousel owl-theme">
                <?php foreach( $this->get( 'products', [] ) as $id => $productItem ) : ?>
                <?php
                $params = array_diff_key( ['d_name' => $productItem->getName( 'url' ), 'd_prodid' => $productItem->getId(), 'd_pos' => $position !== null ? $position++ : ''], $detailFilter );

                $disabled = '';
                $curdate = date( 'Y-m-d H:i:00' );

                if( ( $startDate = $productItem->getDateStart() ) !== null && $startDate > $curdate
                    || ( $endDate = $productItem->getDateEnd() ) !== null && $endDate < $curdate
                ) {
                    $disabled = 'disabled';
                }
                ?>
                <div class="item">
                    <div class="product-details">
                        <div class="product-media">
                            <?php $mediaItems = $productItem->getRefItems( 'media', 'default', 'default' ) ?>
                            <?php if( ( $mediaItem = current( $mediaItems ) ) !== false ) : ?>
                                <img src="<?= $enc->attr( $this->content( $mediaItem->getPreview() ) ); ?>"
                                     alt="<?= $enc->attr( $mediaItem->getName() ); ?>"
                                />
                            <?php endif; ?>
                            <?php if( ( $mediaItem = next( $mediaItems ) ) !== false ) : ?>
                                <span class="hover-image white-bg">
                                    <img alt="" src="<?= $enc->attr( $this->content( $mediaItem->getPreview() ) ); ?>" alt="<?= $enc->attr( $mediaItem->getName() ); ?>">
                                    <meta itemprop="contentUrl" content="<?= $enc->attr( $this->content( $mediaItem->getPreview() ) ); ?>" />
                                </span>
                            <?php endif;?>
                        </div>
                        <div class="product-content">
                            <a href="<?= $enc->attr( $this->url( ( $productItem->getTarget() ?: $detailTarget ), $detailController, $detailAction, $params, [], $detailConfig ) ); ?>">
                            <div class="product-name">
                                <p>

                                        <?= $enc->html( $productItem->getName(), $enc::TRUST ); ?>

                                </p>
                            </div>
                            <div class="product-price">
                                <div class="price-list">
                                    <div class="articleitem price price-actual"
                                         data-prodid="<?= $enc->attr( $productItem->getId() ); ?>"
                                         data-prodcode="<?= $enc->attr( $productItem->getCode() ); ?>">
                                        <?php $priceItems = $productItem->getRefItems( 'price', null, 'default' ); ?>
                                        <?= $this->partial(
                                        /** client/html/common/partials/price
                                         * Relative path to the price partial template file
                                         *
                                         * Partials are templates which are reused in other templates and generate
                                         * reoccuring blocks filled with data from the assigned values. The price
                                         * partial creates an HTML block for a list of price items.
                                         *
                                         * The partial template files are usually stored in the templates/partials/ folder
                                         * of the core or the extensions. The configured path to the partial file must
                                         * be relative to the templates/ folder, e.g. "partials/price-standard.php".
                                         *
                                         * @param string Relative path to the template file
                                         * @since 2015.04
                                         * @category Developer
                                         */
                                            $this->config( 'client/html/common/partials/price', 'common/partials/price-standard' ),
                                            array( 'prices' => reset( $priceItems ) ?: [] )
                                        ); ?>
                                    </div>

                                    <?php if( $productItem->getType() === 'select' ) : ?>
                                        <?php foreach( $productItem->getRefItems( 'product', 'default', 'default' ) as $prodid => $product ) : ?>
                                            <?php if( isset( $productItems[$prodid] ) ) { $product = $productItems[$prodid]; } ?>

                                            <?php if( ( $prices = $product->getRefItems( 'price', null, 'default' ) ) !== [] ) : ?>
                                                <div class="articleitem price"
                                                     data-prodid="<?= $enc->attr( $prodid ); ?>"
                                                     data-prodcode="<?= $enc->attr( $product->getCode() ); ?>">
                                                    <?= $this->partial(
                                                        $this->config( 'client/html/common/partials/price', 'common/partials/price-standard' ),
                                                        array( 'prices' => $prices )
                                                    ); ?>
                                                </div>
                                            <?php endif; ?>

                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                            </a>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
