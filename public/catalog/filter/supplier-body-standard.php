<?php

/**
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @copyright Aimeos (aimeos.org), 2018
 */

$enc = $this->encoder();

$contentUrl = $this->config( 'resource/fs/baseurl' );

$listTarget = $this->config( 'client/html/catalog/lists/url/target' );
$listController = $this->config( 'client/html/catalog/lists/url/controller', 'catalog' );
$listAction = $this->config( 'client/html/catalog/lists/url/action', 'list' );
$listConfig = $this->config( 'client/html/catalog/lists/url/config', [] );

$suppliers = $this->get( 'supplierList', [] );
$supIds = $this->param( 'f_supid', [] );
$params = $this->param();


?>
<?php $this->block()->start( 'catalog/filter/supplier' ); ?>
<div class="sidebar-widget light-bg default-box-shadow">

	<?php if( !empty( $suppliers ) ) : ?>
		<h4 class="widget-title pink-bg">
            <span>
                <?= $enc->html( $this->translate( 'client', 'Suppliers' ), $enc::TRUST ); ?>
            </span>
        </h4>

		<div class="widget-content">
			<ul id="pink-scroll">

				<?php foreach( $suppliers as $id => $supplier ) : ?>
					<li data-id="<?= $enc->attr( $id ); ?>">
                        <label class="checkbox-inline" for="sup-<?= $enc->attr( $id ); ?>">
                            <input class="attr-item" type="checkbox"
                                   id="sup-<?= $enc->attr( $id ); ?>"
                                   name="<?= $enc->attr( $this->formparam( ['f_supid', ''] ) ); ?>"
                                   value="<?= $enc->attr( $id ); ?>"
                                <?= ( in_array( $id, $supIds ) ? 'checked="checked"' : '' ); ?>
                            />
                            <span><?= $enc->html( $supplier->getName(), $enc::TRUST ); ?></span>
                        </label>
					</li>

				<?php endforeach; ?>
			</ul>
		</div>

	<?php endif; ?>


	<?php if( $this->config( 'client/html/catalog/filter/standard/button', true ) ) : ?>
		<noscript>
			<button class="filter btn btn-primary" type="submit">
				<?= $enc->html( $this->translate( 'client', 'Show' ), $enc::TRUST ); ?>
			</button>
		</noscript>
	<?php endif; ?>

</div>
<?php $this->block()->stop(); ?>
<?= $this->block()->get( 'catalog/filter/supplier' ); ?>



