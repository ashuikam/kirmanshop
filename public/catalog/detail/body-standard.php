<?php

/**
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @copyright Metaways Infosystems GmbH, 2012
 * @copyright Aimeos (aimeos.org), 2015-2018
 */

/* Available data:
 * - detailProductItem : Product item incl. referenced items
 * - detailProductItems : Referenced products (bundle, variant, suggested, bought, etc) incl. referenced items
 * - detailParams : Request parameters for this detail view
 */


$getProductList = function( array $posItems, array $items )
{
	$list = [];

	foreach( $posItems as $id => $posItem )
	{
		if( isset( $items[$id] ) ) {
			$list[$id] = $items[$id];
		}
	}

	return $list;
};


$enc = $this->encoder();

$optTarget = $this->config( 'client/jsonapi/url/target' );
$optCntl = $this->config( 'client/jsonapi/url/controller', 'jsonapi' );
$optAction = $this->config( 'client/jsonapi/url/action', 'options' );
$optConfig = $this->config( 'client/jsonapi/url/config', [] );

$basketTarget = $this->config( 'client/html/basket/standard/url/target' );
$basketController = $this->config( 'client/html/basket/standard/url/controller', 'basket' );
$basketAction = $this->config( 'client/html/basket/standard/url/action', 'index' );
$basketConfig = $this->config( 'client/html/basket/standard/url/config', [] );
$basketSite = $this->config( 'client/html/basket/standard/url/site' );

$basketParams = ( $basketSite ? ['site' => $basketSite] : [] );


/** client/html/basket/require-stock
 * Customers can order products only if there are enough products in stock
 *
 * Checks that the requested product quantity is in stock before
 * the customer can add them to his basket and order them. If there
 * are not enough products available, the customer will get a notice.
 *
 * @param boolean True if products must be in stock, false if products can be sold without stock
 * @since 2014.03
 * @category Developer
 * @category User
 */
$reqstock = (int) $this->config( 'client/html/basket/require-stock', true );

$prodItems = $this->get( 'detailProductItems', [] );

$propMap = $subPropDeps = $propItems = [];
$attrMap = $subAttrDeps = $mediaItems = [];

if( isset( $this->detailProductItem ) )
{
	$propItems = $this->detailProductItem->getPropertyItems();
	$posItems = $this->detailProductItem->getRefItems( 'product', null, 'default' );

	if( in_array( $this->detailProductItem->getType(), ['bundle', 'select'] ) )
	{
		foreach( $getProductList( $posItems, $prodItems ) as $subProdId => $subProduct )
		{
			$subItems = $subProduct->getRefItems( 'attribute', null, 'default' );
			$subItems += $subProduct->getRefItems( 'attribute', null, 'variant' ); // show product variant attributes as well
			$mediaItems = array_merge( $mediaItems, $subProduct->getRefItems( 'media', 'default', 'default' ) );

			foreach( $subItems as $attrId => $attrItem )
			{
				$attrMap[$attrItem->getType()][$attrId] = $attrItem;
				$subAttrDeps[$attrId][] = $subProdId;
			}

			$propItems = array_merge( $propItems, $subProduct->getPropertyItems() );
		}
	}

	foreach( $propItems as $propItem )
	{
		$propMap[$propItem->getType()][$propItem->getId()] = $propItem;
		$subPropDeps[$propItem->getId()] = $propItem->getParentId();
	}
}


?>

<article class="container theme-container">
    <section  id="product-fullwidth" class="space-bottom-45" itemscope="" itemtype="https://schema.org/Product" data-jsonurl="<?= $enc->attr( $this->url( $optTarget, $optCntl, $optAction, [], [], $optConfig ) ); ?>">

	    <?php if( isset( $this->detailErrorList ) ) : ?>
		    <ul class="error-list">
			    <?php foreach( (array) $this->detailErrorList as $errmsg ) : ?>
				    <li class="error-item"><?= $enc->html( $errmsg ); ?></li>
			    <?php endforeach; ?>
		    </ul>
	    <?php endif; ?>


	    <?php if( isset( $this->detailProductItem ) ) : ?>
		    <?php
			    $conf = $this->detailProductItem->getConfig();

			    $disabled = '';
			    $curdate = date( 'Y-m-d H:i:00' );

			    if( $this->detailProductItem->getType() !== 'event'
				    && ( $startDate = $this->detailProductItem->getDateStart() ) !== null && $startDate > $curdate
				    || ( $endDate = $this->detailProductItem->getDateEnd() ) !== null && $endDate < $curdate
			    ) {
				    $disabled = 'disabled';
			    }
		    ?>

        <div class="single-product-wrap" data-id="<?= $this->detailProductItem->getId(); ?>">
            <div class="list-category-details">
                <div class="row">
			        <div class="col-md-6 col-sm-6">
				        <?= $this->partial(
					        /** client/html/catalog/detail/partials/image
					        * Relative path to the detail image partial template file
					        *
					        * Partials are templates which are reused in other templates and generate
					        * reoccuring blocks filled with data from the assigned values. The image
					        * partial creates an HTML block for the catalog detail images.
					        *
					        * @param string Relative path to the template file
					        * @since 2017.01
					        * @category Developer
					        */
					        $this->config( 'client/html/catalog/detail/partials/image', 'catalog/detail/image-partial-standard' ),
					        array(
						        'productItem' => $this->detailProductItem,
						        'params' => $this->get( 'detailParams', [] ),
						        'mediaItems' => array_merge( $this->detailProductItem->getRefItems( 'media', 'default', 'default' ), $mediaItems )
					        )
				        ); ?>
			        </div>
			        <div class="col-md-6 col-sm-6">
                        <div class="product-content">
                            <div class="product-name">
                                <h1 class="name" itemprop="name"><?= $enc->html( $this->detailProductItem->getName(), $enc::TRUST ); ?></h1>
                            </div>
                            <div class="product-price">
                                <?php if( isset( $this->detailProductItem ) ) : ?>
                                    <div class="price-list">
                                        <div class="articleitem price price-actual"
                                             data-prodid="<?= $enc->attr( $this->detailProductItem->getId() ); ?>"
                                             data-prodcode="<?= $enc->attr( $this->detailProductItem->getCode() ); ?>">
                                            <?= $this->partial(
                                                $this->config( 'client/html/common/partials/price', 'common/partials/price-standard' ),
                                                array( 'prices' => $this->detailProductItem->getRefItems( 'price', null, 'default' ) )
                                            ); ?>
                                        </div>

                                        <?php if( $this->detailProductItem->getType() === 'select' ) : ?>
                                            <?php foreach( $getProductList( $this->detailProductItem->getRefItems( 'product', 'default', 'default' ), $prodItems ) as $prodid => $product ) : ?>
                                                <?php if( ( $prices = $product->getRefItems( 'price', null, 'default' ) ) !== [] ) : ?>
                                                    <div class="articleitem price"
                                                         data-prodid="<?= $enc->attr( $prodid ); ?>"
                                                         data-prodcode="<?= $enc->attr( $product->getCode() ); ?>">
                                                        <?= $this->partial(
                                                            $this->config( 'client/html/common/partials/price', 'common/partials/price-standard' ),
                                                            array( 'prices' => $prices )
                                                        ); ?>
                                                    </div>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="product-availability">
                                        <p class="code">
                                            <span class="name"><?= $enc->html( $this->translate( 'client', 'Article no.' ), $enc::TRUST ); ?>: </span>
                                            <span class="value" itemprop="sku"><?= $enc->html( $this->detailProductItem->getCode() ); ?></span>
                                        </p>
                                <hr class="fullwidth-divider">
                            </div>
                            <div class="product-discription">
                                <?php foreach( $this->detailProductItem->getRefItems( 'text', 'short', 'default' ) as $textItem ) : ?>
                                    <p class="short" itemprop="description"><?= $enc->html( $textItem->getContent(), $enc::TRUST ); ?></p>
                                <?php endforeach; ?>
                            </div>
                            <div class="catalog-detail-basket" data-reqstock="<?= $reqstock; ?>" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                <form method="POST" action="<?= $enc->attr( $this->url( $basketTarget, $basketController, $basketAction, $basketParams, [], $basketConfig ) ); ?>">
                                    <!-- catalog.detail.csrf -->
                                    <?= $this->csrf()->formfield(); ?>
                                    <!-- catalog.detail.csrf -->

                                    <?php if( $basketSite ) : ?>
                                        <input type="hidden" name="<?= $this->formparam( 'site' ) ?>" value="<?= $enc->attr( $basketSite ) ?>" />
                                    <?php endif ?>

                                    <?php if( $this->detailProductItem->getType() === 'select' ) : ?>
                                        <div class="catalog-detail-basket-selection">
                                            <?= $this->partial(
                                            /** client/html/common/partials/selection
                                             * Relative path to the variant attribute partial template file
                                             *
                                             * Partials are templates which are reused in other templates and generate
                                             * reoccuring blocks filled with data from the assigned values. The selection
                                             * partial creates an HTML block for a list of variant product attributes
                                             * assigned to a selection product a customer must select from.
                                             *
                                             * The partial template files are usually stored in the templates/partials/ folder
                                             * be relative to the templates/ folder, e.g. "partials/selection-standard.php".
                                             *
                                             * of the core or the extensions. The configured path to the partial file must
                                             * @param string Relative path to the template file
                                             * @since 2015.04
                                             * @category Developer
                                             * @see client/html/common/partials/attribute
                                             */
                                                $this->config( 'client/html/common/partials/selection', 'common/partials/selection-standard' ),
                                                array(
                                                    'products' => $this->detailProductItem->getRefItems( 'product', 'default', 'default' ),
                                                    'attributeItems' => $this->get( 'detailAttributeItems', [] ),
                                                    'productItems' => $this->get( 'detailProductItems', [] ),
                                                    'mediaItems' => $this->get( 'detailMediaItems', [] ),
                                                    'productItem' => $this->detailProductItem,
                                                )
                                            ); ?>
                                        </div>
                                    <?php endif; ?>

                                    <div class="catalog-detail-basket-attribute">
                                        <?= $this->partial(
                                        /** client/html/common/partials/attribute
                                         * Relative path to the product attribute partial template file
                                         *
                                         * Partials are templates which are reused in other templates and generate
                                         * reoccuring blocks filled with data from the assigned values. The attribute
                                         * partial creates an HTML block for a list of optional product attributes a
                                         * customer can select from.
                                         *
                                         * The partial template files are usually stored in the templates/partials/ folder
                                         * of the core or the extensions. The configured path to the partial file must
                                         * be relative to the templates/ folder, e.g. "partials/attribute-standard.php".
                                         *
                                         * @param string Relative path to the template file
                                         * @since 2016.01
                                         * @category Developer
                                         * @see client/html/common/partials/selection
                                         */
                                            $this->config( 'client/html/common/partials/attribute', 'common/partials/attribute-standard' ),
                                            array(
                                                'productItem' => $this->detailProductItem,
                                                'attributeItems' => $this->get( 'detailAttributeItems', [] ),
                                                'attributeConfigItems' => $this->detailProductItem->getRefItems( 'attribute', null, 'config' ),
                                                'attributeCustomItems' => $this->detailProductItem->getRefItems( 'attribute', null, 'custom' ),
                                                'attributeHiddenItems' => $this->detailProductItem->getRefItems( 'attribute', null, 'hidden' ),
                                            )
                                        ); ?>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <span class="qty-text col-md-2 col-sm-2">Ко-во:</span>
                                        <div class="addbasket col-md-3 col-sm-3">
                                            <div class="input-group">
                                                <input type="hidden" value="add"
                                                       name="<?= $enc->attr( $this->formparam( 'b_action' ) ); ?>"
                                                />
                                                <input type="hidden"
                                                       name="<?= $enc->attr( $this->formparam( array( 'b_prod', 0, 'prodid' ) ) ); ?>"
                                                       value="<?= $enc->attr( $this->detailProductItem->getId() ); ?>"
                                                />
                                                <input type="number" class="form-control input-lg qty" <?= $disabled ?>
                                                       name="<?= $enc->attr( $this->formparam( array( 'b_prod', 0, 'quantity' ) ) ); ?>"
                                                       min="1" max="2147483647" maxlength="10" step="1" required="required" value="1"
                                                />
                                            </div>
                                        </div>
                                        <div class="stock-list col-md-7 col-sm-7">
                                            <div class="articleitem stock-actual"
                                                 data-prodid="<?= $enc->attr( $this->detailProductItem->getId() ); ?>"
                                                 data-prodcode="<?= $enc->attr( $this->detailProductItem->getCode() ); ?>">
                                            </div>
                                            <?php foreach( $this->detailProductItem->getRefItems( 'product', null, 'default' ) as $articleId => $articleItem ) : ?>
                                                <div class="articleitem"
                                                     data-prodid="<?= $enc->attr( $articleId ); ?>"
                                                     data-prodcode="<?= $enc->attr( $articleItem->getCode() ); ?>">
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <hr class="fullwidth-divider">
                                    <div class="add-to-cart col-md-6 col-sm-6" type="submit" value="" <?= $disabled ?>>
                                        <button class="blue-btn btn" href="#"> <i class="fa fa-shopping-cart white-color"></i>
                                            <?= $enc->html( $this->translate( 'client', 'Add to basket' ), $enc::TRUST ); ?>
                                        </button>
                                    </div>
                                </form>
                                <div class="add-to-cart col-md-6 col-sm-6 ">
                                    <?= $this->partial(
                                    /** client/html/catalog/partials/actions
                                     * Relative path to the catalog actions partial template file
                                     *
                                     * Partials are templates which are reused in other templates and generate
                                     * reoccuring blocks filled with data from the assigned values. The actions
                                     * partial creates an HTML block for the product actions (pin, like and watch
                                     * products).
                                     *
                                     * @param string Relative path to the template file
                                     * @since 2017.04
                                     * @category Developer
                                     */
                                        $this->config( 'client/html/catalog/partials/actions', 'catalog/actions-partial-standard' ),
                                        array(
                                            'productItem' => $this->detailProductItem,
                                            'params' => $this->get( 'detailParams', [] )
                                        )
                                    ); ?>
                                </div>
                            </div>
                        </div>

                        <!-- Social -->
					    <?= $this->partial(
					        /** client/html/catalog/partials/social
					        * Relative path to the social partial template file
					        *
					        * Partials are templates which are reused in other templates and generate
					        * reoccuring blocks filled with data from the assigned values. The social
					        * partial creates an HTML block for links to social platforms in the
			    	        * catalog components.
					        *
			    	        * @param string Relative path to the template file
					        * @since 2017.04
					        * @category Developer
					        */
					        $this->config( 'client/html/catalog/partials/social', 'catalog/social-partial-standard' ),
					        array( 'productItem' => $this->detailProductItem )
				        ); ?>

			        </div>
                </div>
            </div>
        </div>
        <section id="description-item">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <!-- Product Best Sellers Start -->
                    <section id="single-product-tabination" class="space-bottom-45">
                        <div class="light-bg product-tabination default-box-shadow">
                            <div class="tabination">
                                <div class="product-tabs" role="tabpanel">
                                    <!-- Nav tabs -->
                                    <ul role="tablist" class="nav nav-tabs navtab-horizontal">
                                            <?php if( ( $textItems = $this->detailProductItem->getRefItems( 'text', 'long' ) ) !== [] ) : ?>
                                            <li  role="presentation" class="active">
                                                <a class="green-background" data-toggle="tab" role="tab"  href="#description" aria-expanded="true">
                                                    <?= $enc->html( $this->translate( 'client', 'Description' ), $enc::TRUST ); ?>
                                                </a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if( count( $attrMap ) > 0 || count( $this->detailProductItem->getRefItems( 'attribute', null, 'default' ) ) > 0 ) : ?>
                                            <li role="presentation">
                                                <a class="pink-background" data-toggle="tab" role="tab"  href="#brand-info" aria-expanded="false">
                                                    <?= $enc->html( $this->translate( 'client', 'Characteristics' ), $enc::TRUST ); ?>
                                                </a>
                                            </li>
                                            <?php endif; ?>
                                            <?php if( count( $propMap ) > 0 ) : ?>
                                            <li role="presentation" class="">
                                                <a data-toggle="tab" class="blue-background" role="tab" href="#faq" aria-expanded="false">
                                                    <?= $enc->html( $this->translate( 'client', 'Properties' ), $enc::TRUST ); ?>
                                                </a>
                                            </li>
                                            <?php endif; ?>
                                            <?php $mediaList = $this->get( 'detailMediaItems', [] ); ?>
                                            <?php if( ( $mediaItems = $this->detailProductItem->getRefItems( 'media', 'download' ) ) !== [] ) : ?>
                                            <li role="presentation" class="">
                                                <a data-toggle="tab" class="golden-background" role="tab" href="#reviews" aria-expanded="false">
                                                    Reviews
                                                </a>
                                            </li>
                                            <?php endif; ?>
                                        </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <!-- =============================== Description ============================= -->
                                        <?php if( ( $textItems = $this->detailProductItem->getRefItems( 'text', 'long' ) ) !== [] ) : ?>
                                            <div id="description" class="tab-pane fade active in" role="tabpanel">
                                                <div class="col-md-12 product-wrap">
                                                    <div class="title-wrap">
                                                        <h2 class="section-title">
                                                                <span>
                                                                    <span class="funky-font blue-tag">
                                                                        <?= $enc->html( $this->translate( 'client', 'Description' ), $enc::TRUST ); ?>
                                                                    </span>
                                                                </span>
                                                        </h2>
                                                    </div>
                                                    <div class="product-disc space-bottom-35">
                                                        <?php foreach( $textItems as $textItem ) : ?>
                                                            <?= $enc->html( $textItem->getContent(), $enc::TRUST ); ?>
                                                        <?php endforeach; ?>

                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                        <!-- ====================== Characteristics Info ======================== -->
                                        <?php if( count( $attrMap ) > 0 || count( $this->detailProductItem->getRefItems( 'attribute', null, 'default' ) ) > 0 ) : ?>
                                            <div id="brand-info" class="tab-pane fade" role="tabpanel">
                                                <div class="col-md-12 product-wrap">
                                                    <div class="title-wrap">
                                                        <h2 class="section-title">
                                                            <span>
                                                                <span class="funky-font blue-tag">
                                                                    <?= $enc->html( $this->translate( 'client', 'Characteristics' ), $enc::TRUST ); ?>
                                                                </span>
                                                            </span>
                                                        </h2>
                                                    </div>
                                                    <div class="product-disc space-bottom-35">
                                                        <div class="content attributes">
                                                            <table class="attributes">
                                                                <tbody>
                                                                <?php foreach( $this->detailProductItem->getRefItems( 'attribute', null, 'default' ) as $attrId => $attrItem ) : ?>
                                                                    <?php if( isset( $attrItems[$attrId] ) ) { $attrItem = $attrItems[$attrId]; } ?>
                                                                    <tr class="item">
                                                                        <td class="name"><?= $enc->html( $this->translate( 'client/code', $attrItem->getType() ), $enc::TRUST ); ?></td>
                                                                        <td class="value">
                                                                            <div class="media-list">
                                                                                <?php foreach( $attrItem->getListItems( 'media', 'icon' ) as $listItem ) : ?>
                                                                                    <?php if( ( $item = $listItem->getRefItem() ) !== null ) : ?>
                                                                                        <?= $this->partial(
                                                                                            $this->config( 'client/html/common/partials/media', 'common/partials/media-standard' ),
                                                                                            array( 'item' => $item, 'boxAttributes' => array( 'class' => 'media-item' ) )
                                                                                        ); ?>
                                                                                    <?php endif; ?>
                                                                                <?php endforeach; ?>
                                                                            </div>
                                                                            <span class="attr-name"><?= $enc->html( $attrItem->getName() ); ?></span>
                                                                            <?php foreach( $attrItem->getRefItems( 'text', 'short' ) as $textItem ) : ?>
                                                                                <div class="attr-short"><?= $enc->html( $textItem->getContent() ); ?></div>
                                                                            <?php endforeach ?>
                                                                            <?php foreach( $attrItem->getRefItems( 'text', 'long' ) as $textItem ) : ?>
                                                                                <div class="attr-long"><?= $enc->html( $textItem->getContent() ); ?></div>
                                                                            <?php endforeach ?>
                                                                        </td>
                                                                    </tr>
                                                                <?php endforeach; ?>
                                                                <?php foreach( $attrMap as $type => $attrItems ) : ?>
                                                                    <?php foreach( $attrItems as $attrItem ) : $classes = ""; ?>
                                                                        <?php
                                                                        if( isset( $subAttrDeps[$attrItem->getId()] ) )
                                                                        {
                                                                            $classes .= ' subproduct';
                                                                            foreach( $subAttrDeps[$attrItem->getId()] as $prodid ) {
                                                                                $classes .= ' subproduct-' . $prodid;
                                                                            }
                                                                        }
                                                                        ?>
                                                                        <tr class="item<?= $classes; ?>">
                                                                            <td class="name"><?= $enc->html( $this->translate( 'client/code', $type ), $enc::TRUST ); ?></td>
                                                                            <td class="value">
                                                                                <div class="media-list">
                                                                                    <?php foreach( $attrItem->getListItems( 'media', 'icon' ) as $listItem ) : ?>
                                                                                        <?php if( ( $item = $listItem->getRefItem() ) !== null ) : ?>
                                                                                            <?= $this->partial(
                                                                                                $this->config( 'client/html/common/partials/media', 'common/partials/media-standard' ),
                                                                                                array( 'item' => $item, 'boxAttributes' => array( 'class' => 'media-item' ) )
                                                                                            ); ?>
                                                                                        <?php endif; ?>
                                                                                    <?php endforeach; ?>
                                                                                </div>
                                                                                <span class="attr-name"><?= $enc->html( $attrItem->getName() ); ?></span>
                                                                                <?php foreach( $attrItem->getRefItems( 'text', 'short' ) as $textItem ) : ?>
                                                                                    <div class="attr-short"><?= $enc->html( $textItem->getContent() ); ?></div>
                                                                                <?php endforeach ?>
                                                                                <?php foreach( $attrItem->getRefItems( 'text', 'long' ) as $textItem ) : ?>
                                                                                    <div class="attr-long"><?= $enc->html( $textItem->getContent() ); ?></div>
                                                                                <?php endforeach ?>
                                                                            </td>
                                                                        </tr>
                                                                    <?php endforeach; ?>
                                                                <?php endforeach; ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                        <!-- ====================== Properties ======================== -->
                                        <?php if( count( $propMap ) > 0 ) : ?>
                                            <div id="faq" class="tab-pane fade" role="tabpanel">
                                                <div class="col-md-12 product-wrap">
                                                    <div class="title-wrap">
                                                        <h2 class="section-title">
                                                                <span>
                                                                    <span class="funky-font blue-tag">

                                                                    </span>
                                                                </span>
                                                        </h2>
                                                    </div>
                                                    <div class="product-disc space-bottom-35">
                                                        <div class="content properties">
                                                            <table class="properties">
                                                                <tbody>
                                                                <?php foreach( $propMap as $type => $propItems ) : ?>
                                                                    <?php foreach( $propItems as $propertyItem ) : $classes = ''; ?>
                                                                        <?php
                                                                        if( isset( $subPropDeps[$propertyItem->getId()] ) ) {
                                                                            $classes .= ' subproduct subproduct-' . $subPropDeps[$propertyItem->getId()];
                                                                        }
                                                                        ?>
                                                                        <tr class="item<?= $classes; ?>">
                                                                            <td class="name"><?= $enc->html( $this->translate( 'client/code', $propertyItem->getType() ), $enc::TRUST ); ?></td>
                                                                            <td class="value"><?= $enc->html( $propertyItem->getValue() ); ?></td>
                                                                        </tr>
                                                                    <?php endforeach; ?>
                                                                <?php endforeach; ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php endif; ?>
                                        <!-- ====================== Reviews ======================== -->
                                        <?php if( ( $mediaItems = $this->detailProductItem->getRefItems( 'media', 'download' ) ) !== [] ) : ?>
                                            <div id="reviews" class="tab-pane fade" role="tabpanel">
                                                <div class="col-md-12 product-wrap">
                                                    <div class="title-wrap">
                                                        <h2 class="section-title">
                                                                <span>
                                                                    <span class="funky-font blue-tag">
                                                                        <?= $enc->html( $this->translate( 'client', 'Downloads' ), $enc::TRUST ); ?>
                                                                    </span>
                                                                </span>
                                                        </h2>
                                                    </div>
                                                    <div class="product-disc space-bottom-35">
                                                        <ul class="content downloads">
                                                            <?php foreach( $mediaItems as $id => $item ) : ?>
                                                                <?php if( isset( $mediaList[$id] ) ) { $item = $mediaList[$id]; } ?>
                                                                <li class="item">
                                                                    <a href="<?= $this->content( $item->getUrl() ); ?>" title="<?= $enc->attr( $item->getName() ); ?>">
                                                                        <img class="media-image"
                                                                             src="<?= $this->content( $item->getPreview() ); ?>"
                                                                             alt="<?= $enc->attr( $item->getName() ); ?>"
                                                                        />
                                                                        <span class="media-name"><?= $enc->html( $item->getName() ); ?></span>
                                                                    </a>
                                                                </li>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- / Product Best Sellers Ends -->
                </div>
                <div class="col-md-4 col-sm-4">
                        <?php if ( ( $posItems = $this->detailProductItem->getRefItems( 'product', null, 'suggestion' ) ) !== [] ||
                        ( $posItems = $this->detailProductItem->getRefItems( 'product', null, 'bought-together' ) ) !== []) : ?>
                        <div class="light-bg padding-25 top-rated default-box-shadow related-product">
                            <div class="row">
                                <div class="title-wrap col-md-9">

                                </div>
                                <div class="poroduct-pagination col-md-3">
                                    <span class="product-slide gray-background next"> <i class="fa fa-chevron-left"></i> </span>
                                    <span class="product-slide gray-background prev"> <i class="fa fa-chevron-right"></i> </span>
                                </div>
                            </div>
                            <div class="top-rated-owl-slider">
                                <div class="item">
                                    <?php if( ( $posItems = $this->detailProductItem->getRefItems( 'product', null, 'suggestion' ) ) !== []
                                        && ( $products = $getProductList( $posItems, $prodItems ) ) !== [] ) : ?>
                                        <section class="catalog-detail-suggest">
                                            <h2 class="header"><?= $this->translate( 'client', 'Suggested products' ); ?></h2>
                                            <?= $this->partial(
                                                $this->config( 'client/html/common/partials/products', 'common/partials/related-standard' ),
                                                array( 'products' => $products, 'itemprop' => 'isRelatedTo' )
                                            ); ?>
                                        </section>

                                    <?php endif; ?>
                                </div>
                                <div class="item">
                                    <?php if( ( $posItems = $this->detailProductItem->getRefItems( 'product', null, 'bought-together' ) ) !== []
                                        && ( $products = $getProductList( $posItems, $prodItems ) ) !== [] ) : ?>
                                        <section class="catalog-detail-bought">
                                            <h2 class="header"><?= $this->translate( 'client', 'Other customers also bought' ); ?></h2>
                                            <?= $this->partial(
                                                $this->config( 'client/html/common/partials/products', 'common/partials/related-standard' ),
                                                array( 'products' => $products, 'itemprop' => 'isRelatedTo' )
                                            ); ?>
                                        </section>

                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
            </div>
        </section>
		<div class="col-sm-12">

				<?php /* if( $this->detailProductItem->getType() === 'bundle'
					&& ( $posItems = $this->detailProductItem->getRefItems( 'product', null, 'default' ) ) !== []
					&& ( $products = $getProductList( $posItems, $prodItems ) ) !== [] ) : ?>
					<section class="catalog-detail-bundle">
						<h2 class="header"><?= $this->translate( 'client', 'Bundled products' ); ?></h2>
						<?= $this->partial(
							$this->config( 'client/html/common/partials/products', 'common/partials/products-standard' ),
							array( 'products' => $products, 'itemprop' => 'isRelatedTo' )
						); ?>
					</section>

				<?php endif; */?>








				<?php //$this->block()->get( 'catalog/detail/supplier' ); ?>

			</div>

	    <?php endif; ?>

    </section>
</article>



    <!-- / Related Products Ends -->

