<?php

/**
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @copyright Metaways Infosystems GmbH, 2013
 * @copyright Aimeos (aimeos.org), 2015-2018
 */

$enc = $this->encoder();

$detailTarget = $this->config( 'client/html/catalog/detail/url/target' );
$detailController = $this->config( 'client/html/catalog/detail/url/controller', 'catalog' );
$detailAction = $this->config( 'client/html/catalog/detail/url/action', 'detail' );
$detailConfig = $this->config( 'client/html/catalog/detail/url/config', [] );
$detailFilter = array_flip( $this->config( 'client/html/catalog/detail/url/filter', ['d_prodid'] ) );


?>
<?php if( isset( $this->seenProductItem ) ) : $productItem = $this->seenProductItem; ?>

	<?php
		$mediaItems = $productItem->getRefItems( 'media', 'default', 'default' );
		$params = array_diff_key( ['d_name' => $productItem->getName( 'url' ), 'd_prodid' => $productItem->getId(), 'd_pos' => ''], $detailFilter );
	?>

	<a href="<?= $enc->attr( $this->url( $detailTarget, $detailController, $detailAction, $params, [], $detailConfig ) ); ?>">
        <div class="product-media">
            <!--span class="hover-image white-bg">
                <img src="assets/img/product/cat-7.png" alt="">
            </span-->
            <?php if( ( $mediaItem = reset( $mediaItems ) ) !== false ) : ?>
                <img src="<?= $this->content( $mediaItem->getPreview() ); ?>" alt="product-img">
            <?php else : ?>
                <div class="media-item"></div>
            <?php endif; ?>
        </div>
        <div class="product-content">
            <div class="product-name">
                <p class="name"><?= $enc->html( $productItem->getName(), $enc::TRUST ); ?></p>
            </div>
            <div class="price-list">
                <?= $this->partial(
                    $this->config( 'client/html/common/partials/price', 'common/partials/price-standard' ),
                    array( 'prices' => $productItem->getRefItems( 'price', null, 'default' ) )
                ); ?>
            </div>
        </div>
	</a>
<?php endif; ?>


