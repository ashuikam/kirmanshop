<?php

/**
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @copyright Metaways Infosystems GmbH, 2013
 * @copyright Aimeos (aimeos.org), 2015-2018
 */

$enc = $this->encoder();

$optTarget = $this->config( 'client/jsonapi/url/target' );
$optCntl = $this->config( 'client/jsonapi/url/controller', 'jsonapi' );
$optAction = $this->config( 'client/jsonapi/url/action', 'options' );
$optConfig = $this->config( 'client/jsonapi/url/config', [] );


?>
<section id="product-tabination-1" class="space-bottom-45" data-jsonurl="<?= $enc->attr( $this->url( $optTarget, $optCntl, $optAction, [], [], $optConfig ) ); ?>">

	<?php if( isset( $this->sessionErrorList ) ) : ?>
		<ul class="error-list">
			<?php foreach( (array) $this->sessionErrorList as $errmsg ) : ?>
				<li class="error-item"><?= $enc->html( $errmsg ); ?></li>
			<?php endforeach; ?>
		</ul>
	<?php endif; ?>



    <div class="container theme-container">
        <div class="title-wrap with-border space-25">
            <h2 class="section-title with-border">
                    <span class="white-bg">
                        <span class="funky-font blue-tag">Недавно</span>
                        <span class="italic-font">смотрели</span>
                    </span>
            </h2>
            <h3 class="sub-title">Недавно просмотренные товары</h3>
            <hr class="dash-divider">
        </div>



        <div class="light-bg product-tabination">
            <div class="tabination">
                <div class="product-tabs" role="tabpanel">
                    <!-- Nav tabs -->
                    <ul role="tablist" class="nav nav-tabs navtab-horizontal">
                        <li role="presentation" class="active">
                            <a class="green-background" data-toggle="tab" role="tab"  href="#recently-viewed" aria-expanded="true">
                                <?= $this->translate( 'client', 'Last seen' ); ?>
                            </a>
                        </li>
                        <li class="" role="presentation">
                            <a class="pink-background" data-toggle="tab" role="tab"  href="#related-product" aria-expanded="false">
                                <?= $this->translate( 'client', 'Pinned products' ); ?>
                            </a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <!-- ====================== Recently Viewed ======================== -->
                        <div id="recently-viewed" class="tab-pane fade active in" role="tabpanel">
                            <?= $this->block()->get( 'catalog/session/seen' ); ?>
                        </div>
                        <!-- ====================== Related Products ======================== -->
                        <div id="related-product" class="tab-pane fade" role="tabpanel">
                            <?= $this->block()->get( 'catalog/session/pinned' ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

