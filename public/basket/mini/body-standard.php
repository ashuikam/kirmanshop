<?php

/**
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @copyright Metaways Infosystems GmbH, 2013
 * @copyright Aimeos (aimeos.org), 2015-2018
 */

$enc = $this->encoder();


/** client/html/basket/standard/url/target
 * Destination of the URL where the controller specified in the URL is known
 *
 * The destination can be a page ID like in a content management system or the
 * module of a software development framework. This "target" must contain or know
 * the controller that should be called by the generated URL.
 *
 * @param string Destination of the URL
 * @since 2014.03
 * @category Developer
 * @see client/html/basket/standard/url/controller
 * @see client/html/basket/standard/url/action
 * @see client/html/basket/standard/url/config
 * @see client/html/basket/standard/url/site
 */
$basketTarget = $this->config( 'client/html/basket/standard/url/target' );

/** client/html/basket/standard/url/controller
 * Name of the controller whose action should be called
 *
 * In Model-View-Controller (MVC) applications, the controller contains the methods
 * that create parts of the output displayed in the generated HTML page. Controller
 * names are usually alpha-numeric.
 *
 * @param string Name of the controller
 * @since 2014.03
 * @category Developer
 * @see client/html/basket/standard/url/target
 * @see client/html/basket/standard/url/action
 * @see client/html/basket/standard/url/config
 * @see client/html/basket/standard/url/site
 */
$basketController = $this->config( 'client/html/basket/standard/url/controller', 'basket' );

/** client/html/basket/standard/url/action
 * Name of the action that should create the output
 *
 * In Model-View-Controller (MVC) applications, actions are the methods of a
 * controller that create parts of the output displayed in the generated HTML page.
 * Action names are usually alpha-numeric.
 *
 * @param string Name of the action
 * @since 2014.03
 * @category Developer
 * @see client/html/basket/standard/url/target
 * @see client/html/basket/standard/url/controller
 * @see client/html/basket/standard/url/config
 * @see client/html/basket/standard/url/site
 */
$basketAction = $this->config( 'client/html/basket/standard/url/action', 'index' );

/** client/html/basket/standard/url/config
 * Associative list of configuration options used for generating the URL
 *
 * You can specify additional options as key/value pairs used when generating
 * the URLs, like
 *
 *  client/html/<clientname>/url/config = array( 'absoluteUri' => true )
 *
 * The available key/value pairs depend on the application that embeds the e-commerce
 * framework. This is because the infrastructure of the application is used for
 * generating the URLs. The full list of available config options is referenced
 * in the "see also" section of this page.
 *
 * @param string Associative list of configuration options
 * @since 2014.03
 * @category Developer
 * @see client/html/basket/standard/url/target
 * @see client/html/basket/standard/url/controller
 * @see client/html/basket/standard/url/action
 * @see client/html/basket/standard/url/site
 * @see client/html/url/config
 */
$basketConfig = $this->config( 'client/html/basket/standard/url/config', [] );

/** client/html/basket/standard/url/site
 * Locale site code where products will be added to the basket
 *
 * In more complex setups with several shop sites, this setting allows to to
 * define the shop site that will manage the basket of the customer. For example
 * in market place setups where all vendors have there own shop sites, the basket
 * site should be the site code of the market place ("default" by default).
 *
 * @param string Code of the locale site
 * @since 2018.04
 * @category Developer
 * @see client/html/basket/standard/url/target
 * @see client/html/basket/standard/url/controller
 * @see client/html/basket/standard/url/config
 */
$basketSite = $this->config( 'client/html/basket/standard/url/site' );

$basketParams = ( $basketSite ? ['site' => $basketSite] : [] );


$jsonTarget = $this->config( 'client/jsonapi/url/target' );
$jsonController = $this->config( 'client/jsonapi/url/controller', 'jsonapi' );
$jsonAction = $this->config( 'client/jsonapi/url/action', 'options' );
$jsonConfig = $this->config( 'client/jsonapi/url/config', [] );


/// Price format with price value (%1$s) and currency (%2$s)
$priceFormat = $this->translate( 'client', '%1$s %2$s' );


?>

<div class="header-cart col-md-3  col-sm-4">
<section id="basket-mini" data-jsonurl="<?= $enc->attr( $this->url( $jsonTarget, $jsonController, $jsonAction, $basketParams, [], $jsonConfig ) ); ?>">

    <?php if( ( $errors = $this->get( 'miniErrorList', [] ) ) !== [] ) : ?>
	    <ul class="error-list">
		    <?php foreach( $errors as $error ) : ?>
                <li class="error-item"><?= $enc->html( $error ); ?></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
        <div class="cart-wrapper">
	        <?php if( isset( $this->miniBasket ) ) : ?>
		        <?php
			        $quantity = 0;
			        $priceItem = $this->miniBasket->getPrice();
			        $priceCurrency = $this->translate( 'currency', $priceItem->getCurrencyId() );

			        foreach( $this->miniBasket->getProducts() as $product ) {
				        $quantity += $product->getQuantity();
			        }
		        ?>
            <a href="javascript:void(0)" class="btn cart-btn default-btn">
                <i class="fa fa-shopping-cart blue-color"></i>
                <span>
                    <b><?= $enc->html( $this->translate( 'client', 'Basket' ), $enc::TRUST ); ?></b>
                </span>
                <span id="basket-quantity" class="blue-color">
                    <strong><?= $enc->html( $quantity ); ?></strong>
                </span>
                <span id="basket-value" style="display: none">
					<?= $enc->html( sprintf( $priceFormat, $this->number( $priceItem->getValue() + $priceItem->getCosts(), $priceItem->getPrecision() ), $priceCurrency ) ); ?>
				</span>
                <span class="fa fa-caret-down"></span>
            </a>
            <div class="cart-dropdown bg2-with-mask">
                    <span class="blue-color-mask blue-box-shadow color-mask-radius"></span>
                    <div class="pos-relative">
                        <table class="cart-table basket">
                            <tbody id="basket-body" class="basket-body">
                            <tr class="product prototype">
                                <td>
                                    <div class="product-media">
                                        <img class="detail" src="" alt="">
                                    </div>
                                </td>
                                <td>
                                    <div class="product-content">
                                        <div class="product-name">
                                        </div>
                                        <div class="quantity-label">Кол-во:</div>
                                        <div class="quantity"></div>
                                        <h5 class="price"></h5>
                                        <a class="delete fa fa-close" href="#"></a>
                                    </div>
                                </td>
                            </tr>
                            <?php foreach( $this->miniBasket->getProducts() as $pos => $product ) : ?>
                                <?php
                                $param = ['resource' => 'basket', 'id' => 'default', 'related' => 'product', 'relatedid' => $pos];
                                if( $basketSite ) { $param['site'] = $basketSite; }
                                ?>
                                <tr class="product" data-url="<?= $enc->attr( $this->url( $jsonTarget, $jsonController, $jsonAction, $param, [], $jsonConfig ) ); ?>"
                                    data-urldata="<?= $enc->attr( $this->csrf()->name() . '=' . $this->csrf()->value() ); ?>">
                                    <td>
                                        <div class="product-media">
                                            <?php if( ( $url = $product->getMediaUrl() ) != '' ) : ?>
                                                <img class="detail" src="<?= $enc->attr( $this->content( $url ) ); ?>" />
                                            <?php endif; ?>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="product-content">
                                            <div class="product-name">
                                                <?= $enc->html( $product->getName() ) ?>
                                                <!--span> Category </span-->
                                            </div>
                                            <div class="quantity-label">Кол-во: </div>
                                            <div class="quantity">
                                                <?= $enc->html( $product->getQuantity() ) ?>
                                            </div>
                                            <h5 class="price">
                                                    <?= $enc->html( sprintf( $priceFormat, $this->number( $product->getPrice()->getValue(), $product->getPrice()->getPrecision() ), $priceCurrency ) ); ?>*
                                            </h5>
                                            <?php if( ( $product->getFlags() & \Aimeos\MShop\Order\Item\Base\Product\Base::FLAG_IMMUTABLE ) == 0 ) : ?>
                                                <a class="delete fa fa-close" href="#"></a>
                                            <?php endif; ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td>
                                    <span class="hide">
                                        <?= $enc->html( $this->translate( 'client', 'Shipping' ), $enc::TRUST ); ?>
                                    </span>
                                    <span id="delivery" class="hide">
                                    <?= $enc->html( sprintf( $priceFormat, $this->number( $priceItem->getCosts(), $priceItem->getPrecision() ), $priceCurrency ) ); ?>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="sub-total">
                                        <span class="title"><?= $enc->html( $this->translate( 'client', 'Total' ), $enc::TRUST ); ?></span>
                                        <span id="total" class="amount">
                                            <?= $enc->html( sprintf( $priceFormat, $this->number( $priceItem->getValue() + $priceItem->getCosts(), $priceItem->getPrecision() ), $priceCurrency ) ); ?>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                        <div class="chk-out">
                            <a href="<?= $enc->attr( $this->url( $basketTarget, $basketController, $basketAction, $basketParams, [], $basketConfig ) ); ?>" class="btn default-btn">
                                <?= $enc->html( $this->translate( 'client', 'Checkout' ), $enc::TRUST ); ?>
                            </a>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
</section>
</div>





